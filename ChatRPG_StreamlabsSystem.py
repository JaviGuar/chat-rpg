#---------------------------------------
# Libraries and references
#---------------------------------------
import math
import time
import random
import codecs
import json
import os
import winsound
import ctypes
import clr
clr.AddReference("IronPython.Modules.dll")
clr.AddReference("IronPython.SQLite.dll")
import sqlite3
import datetime
#---------------------------------------
# [Required] Script information
#---------------------------------------
ScriptName = "ChatRPG"
Website = "https://www.twitch.tv/javi_guar"
Creator = "javi_guar"
# With help from: Candywolf949, Jpow1659, Maquinas, Mechacatten, Petee3e, Soal5367 
Version = "3.0"
Description = "RPG game for chat with dungeon crawling and raid bosses"
#---------------------------------------
# Versions
#---------------------------------------
"""
1.0 - Release
1.1 - Fixed gain 0 exp problems
    - Locked playing while enlisted for a boss fight 
    - Added message of awarded exp after boss fight 
    - Added stat gain loot to bosses 
    - Rebalanced Bosses
1.2 - Changed info and stats check response to whisper
    - Solo mode difficulty increased and message new boss stats
    - Added a level requirement entry for the bosses
    - Buffed dodge and crit chances
    - Added variable text for stat boost loot
1.3 - Replaced permadeath with stats and exp loss on knock out
    - Rebalance of Defense to reduce a maximum of 75% of the incoming attack
1.4 - Divided bossfights into 4 difficulties, and added separate cooldowns for each one
    - Added a level lock to the call boss command
    - If a boss retires without a fight, the cooldown is not set
1.5 - Increased dungeon loot drop rates
    - Reduced bosses exp loot
1.6 - Added 1v1 arena
2.0 - Added 21 classes with different stat boosts and abilities:
    - Adventurer (no class)
    - Fighter -> (Warrior -> Gladiator) or -> (Protector -> Knight)
    - Scout -> (Rogue -> Assassin) or -> (Archer -> Marksman)
    - Mage -> (Wizard -> Sage) or -> (Illusionist -> Hypnotist)
    - Cleric -> (Priest -> Bishop) or -> (Guardian -> Paladin)
    - Added aggro system
    - Added 3 standby stages in combat for abilities resolution (mechanical)
    - Added class upgrade command
    - Removed stats and exp loss on death
    - Added reset count to player database for future implementation
    - Moved dungeon outcome messages to whispers, loot pop up is still in general chat
2.1 - Scout passive decreased by 20%
    - Mage passive increased by 100%
    - Priest heal formula rebalanced to (heal value * (((personal aggro * players fighting) / total aggro from players) rounded up))
    - Max level increased to 25
    - Requirements for prestige changed from level 20 and full stats to just level 25
    - Fixed some text errors
    - Prestige levels increase the experience and stats you gain in 1/2 times your prestige level
2.2 - Fixed prestige exp bonus
2.3 - Moved a bunch of messages to whispers
2.4 - New class: Druid
    - Druid -> (Naturalist -> Archdruid) or -> (Shapeshifter -> Ironfur)
2.5 - Rebalanced rest to be a percentage of your hp and stm
    - Added a command to check all bosses cooldowns
    - Nerfed Naturalist & Archdruid self healing
    - Prestige bonus reworked to static value
    - Level requirement for bosses reworked
    - Bosses titles changed to normal kill
2.6 - Battle summary added
    - Arena removed
    - Most messages shortened or moved to whispers
3.0 - Classes rework
    - Added Twitch/Discord mode
    - Damage from dungeons are now a percent of your max hp
"""
#---------------------------------------
# Variables
#---------------------------------------
settingsFile = os.path.join(os.path.dirname(__file__), "RPGConfig.json")
#---------------------------------------
# Classes
#---------------------------------------
class Settings:
    """" Loads settings from file if file is found if not uses default values"""

    # The 'default' variable names need to match UI_Config
    def __init__(self, settingsFile=None):
        if settingsFile and os.path.isfile(settingsFile):
            with codecs.open(settingsFile, encoding='utf-8-sig', mode='r') as f:
                self.__dict__ = json.load(f, encoding='utf-8-sig')

        else: #set variables if no custom settings file is found
            self.Mode = 0 # 0 = Twitch, 1 = Discord
            self.InfoCommand = "!rpginfo"
            self.Info = "A link to the chat RPG commands and classes can be found here: https://docs.google.com/document/d/10WO2YTRINZj9hOguiAUkCWAAkf6VZ-l5f83MUstNH28"
            self.NewCharacterCommand = "!rpgnew"
            self.StatsCommand = "!rpgstats"
            self.DungeonCommand = "!rpgdungeon"
            self.BossCommand = "!rpgboss"
            self.CallBossCommand = "!rpgcallboss"
            self.AllBossCD = "!rpgbosscd"
            self.RestCommand = "!rpgrest"
            self.ClassChange = "!classchange"
            self.PrestigeCommand = "!prestige"
            self.WorkCommand = "!rpgwork"
            self.ShopCommand = "!rpgshop"
            self.TitlesCommand = "!rpgtitles"
            self.Permission = "Everyone"
            self.PermissionResp = ""
            self.BossDelay = 120
            self.UseBossCooldown = True
            self.BossCooldown = 1800
            self.BossOnCooldown = "There is no boss to fight right now, rumour says one may appear in {0} minutes and {1} seconds!"
            self.Cost = 50
            self.UseDungeonCooldown = True
            self.DungeonCrawlUserCooldown = 300
            self.UserDungeonOnCooldown = "{0} you are still tired from your last adventure, {1} minutes and {2} seconds until you're ready!"
            self.ResponseNotEnoughPoints = "Unfortunately, {0} doesn't have {1} {2} for basic dungeon supplies."
            self.RestHP = 50
            self.RestStm = 75
            self.UseRestCooldown = True
            self.RestUserCooldown = 1200
            self.UserRestOnCooldown = "{0} you can't rest again that soon!, {1} minutes and {2} seconds until you can rest again!"


    # Reload settings on save through UI
    def ReloadSettings(self, data):
        """Reload settings on save through UI"""
        self.__dict__ = json.loads(data, encoding='utf-8-sig')
        return

    # Save settings to files (json and js)
    def SaveSettings(self, settingsFile):
        """Save settings to files (json and js)"""
        with codecs.open(settingsFile, encoding='utf-8-sig', mode='w+') as f:
            json.dump(self.__dict__, f, encoding='utf-8-sig')
        with codecs.open(settingsFile.replace("json", "js"), encoding='utf-8-sig', mode='w+') as f:
            f.write("var settings = {0};".format(json.dumps(self.__dict__, encoding='utf-8-sig', ensure_ascii=False)))
        return

class TitleList:
    def __init__(self):
        self.Titles = []

    def add_title(self, title):
        Len = len(self.Titles)
        if Len > 0:
            i = 0
            while i < Len and (self.Titles[i] != title):
                if self.Titles[i] != title:
                    i+= 1
            if i < Len:
                return False
        if title != "":
            self.Titles.append(title)
        return True

    def title_array(self):
        Len = len(self.Titles)
        Titles = ''
        if Len > 0:
            i = 0
            while i < Len :
                if i < (Len - 1):
                    Titles += (self.Titles[i] + ', ')
                else:
                    Titles += (self.Titles[i]) 
                i+= 1
        return Titles

#-----------------------------------------Character classes-----------------------------------------
#-----------------------------------------Adventurer-----------------------------------------
class Player(object):
    def __init__(self, userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash):
        self.Class = "Adventurer"
        self.UserId = userId
        self.Name = pname
        self.Exp = pexp
        self.Hp = php
        self.Stm = pstm
        self.Dmg = pdmg
        self.Def = pdef
        self.Dex = pdex
        self.Eva = peva
        self.KillCount = kc
        self.ResetCount = rc
        self.Titles = titles
        self.Currency = currency
        self.DungeonBonus = 0
        self.LootBonus = 0
        self.Max_Hp = 50
        self.Max_Stm = 100
        self.Consumable = consumable
        self.Stash = stash
        self.BonusDmg = 0
        self.BonusDef = 0
        self.BonusEva = 0
        self.BonusDex = 0

    def get_dmg(self):
        return self.Dmg + self.BonusDmg

    def get_def(self):
        return self.Def + self.BonusDef

    def get_dex(self):
        return self.Dex + self.BonusDex

    def get_eva(self):
        return self.Eva + self.BonusEva

    def get_max_stm(self):
        return self.Max_Stm

    def get_max_hp(self):
        return self.Max_Hp

    def get_dodge(self):
        return int(6.0 * math.sqrt(self.get_eva()))

    def get_crit(self):
        return int(6.0 * math.sqrt(self.get_dex()))

    def get_prestige_bonus(self):
        rc = self.ResetCount
        if rc > 5:
            rc = 5
        return rc

    def get_currency(self):
        return self.Currency

    def add_currency(self, n):
        self.Currency += n
        if self.Currency < 0:
            self.Currency = 0

    def add_boss_kill(self):
        self.KillCount += 1
        if self.KillCount >= 500:
            self.gain_title("Monster hunter")

    def add_exp(self, pexp):
        pastExp = self.Exp
        bonus = self.get_prestige_bonus() * 4
        self.Exp += int(pexp + bonus)
        if pastExp < 4:
            if self.Exp >= 4:
                message = "You have a class upgrade available!"
                if MySet.Mode == 0:
                    Parent.SendStreamWhisper(self.UserId, message)
                elif MySet.Mode == 1:
                    Parent.SendDiscordDM(self.UserId, message)
        if pastExp < 121:
            if self.Exp >= 121:
                message = "You have a class upgrade available!"
                if MySet.Mode == 0:
                    Parent.SendStreamWhisper(self.UserId, message)
                elif MySet.Mode == 1:
                    Parent.SendDiscordDM(self.UserId, message)
        if pastExp < 441:
            if self.Exp >= 441:
                self.gain_title('Veteran hero')
        if self.Exp > 441:
            self.Exp = 441
        return (self.Exp - pastExp)

    def add_hp(self, php):
        pastHp = self.Hp
        Max_Hp = self.get_max_hp()
        self.Hp += php
        if self.Hp > Max_Hp:
            self.Hp = Max_Hp
        if self.Hp < 0:
            self.Hp = 0
        return (self.Hp - pastHp)

    def add_stm(self, pstm):
        pastStm = self.Stm
        Max_Stm = self.get_max_stm()
        self.Stm += pstm
        if self.Stm > Max_Stm:
            self.Stm = Max_Stm
        if self.Stm < 0:
            self.Stm = 0
        return (self.Stm - pastStm)

    def add_dmg(self, pdmg):
        pastDmg = self.Dmg
        bonus = self.get_prestige_bonus() * 2
        self.Dmg += int(pdmg + bonus)
        if self.Dmg > 50:
            self.Dmg = 50
        return (self.Dmg - pastDmg)

    def add_def(self, pdef):
        pastDef = self.Def
        bonus = self.get_prestige_bonus() * 2
        self.Def += int(pdef + bonus)
        if self.Def > 50:
            self.Def = 50
        return (self.Def - pastDef)

    def add_dex(self, pdex):
        pastDex = self.Dex
        bonus = self.get_prestige_bonus() * 2
        self.Dex += int(pdex + bonus)
        if self.Dex > 50:
            self.Dex = 50
        return (self.Dex - pastDex)

    def add_eva(self, peva):
        pastEva = self.Eva
        bonus = self.get_prestige_bonus() * 2
        self.Eva += int(peva + bonus)
        if self.Eva > 50:
            self.Eva = 50
        return (self.Eva - pastEva)

    def level_calc(self):
        if self.Exp > 0:
            lvl = int(math.sqrt(self.Exp)-1)
            return lvl
        else:
            return 0

    def death_penalty(self):
        self.cleanup()
        LossMessage = ""
        self.Hp = 1
        self.Stm = 0
        return LossMessage

    def get_titles(self):
        return self.Titles.title_array()

    def defend(self, dmg):
        Dodge = Parent.GetRandom(0,100)
        if Dodge < self.get_dodge():
            return 0
        DmgTaken = 0
        Def = self.get_def()
        MaxDmgBlock = int(dmg * 0.75)
        if Def > MaxDmgBlock:
            DmgTaken = dmg - MaxDmgBlock
        else:
            DmgTaken = dmg - Def
        return DmgTaken

    def attack(self):
        crit_count = 0
        damage = self.get_dmg()
        Crit = Parent.GetRandom(0,100)
        if Crit < self.get_crit():
            crit_count = 1
            damage += damage - int(damage * 0.5)
        return (damage, crit_count)

    def standby(self, Allies):
        #extra step for heals and party effects. Allies is a list of players
        return 0

    def standby_pure(self):
        #extra step for pure damage abilities, returns pure damage
        return 0

    def standby_attack(self, boss_def):
        #extra step for attacks that go against defense, returns damage of the attack
        return (0, 0)

    def cleanup(self):
        #extra step to remove buffs after battle is over or when the player dies
        return 0

    def gain_title(self, title):
        if self.Titles.add_title(title):
            message = ('You earned the title ' + title)
            if MySet.Mode == 0:
                Parent.SendStreamWhisper(self.UserId, message)
            elif MySet.Mode == 1:
                Parent.SendDiscordDM(self.UserId, message)

    def equip(self, item_index):
        Len = len(self.Stash)
        if item_index < Len:
            Held_item = self.Consumable 
            self.Stash.remove_item(AllConsumables[item_index])
            self.Consumable = AllConsumables[item_index]
            if Held_item != None:
                self.Stash.add_item(Held_item)

#-----------------------------------------Warrior (1)-----------------------------------------
class Warrior(Player):
    def __init__(self, userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash):
        super(Warrior, self).__init__(userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash)
        self.Class = "Warrior"

    def get_dmg(self):
        mult = self.level_calc()
        if mult > 10:
            mult = 10
        return super(Warrior, self).get_dmg() + (2 * mult)

    def get_def(self):
        mult = self.level_calc()
        if mult > 10:
            mult = 10
        return super(Warrior, self).get_def() + (2 * mult)

    def get_dex(self):
        mult = self.level_calc()
        if mult > 10:
            mult = 10
        return super(Warrior, self).get_dex() + (0 * mult)

    def get_eva(self):
        mult = self.level_calc()
        if mult > 10:
            mult = 10
        return super(Warrior, self).get_eva() + (0 * mult)

    def get_max_stm(self):
        return super(Warrior, self).get_max_stm() + 50

    def get_max_hp(self):
        mult = self.level_calc()
        if mult > 10:
            mult = 10
        return super(Warrior, self).get_max_hp() + (2 * mult)

#-----------------------------------------Barbarian (10)-----------------------------------------
class Barbarian(Warrior):
    def __init__(self, userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash):
        super(Barbarian, self).__init__(userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash)
        self.Class = "Barbarian"
        self.Enrage = 0

    def get_dmg(self):
        return super(Barbarian, self).get_dmg() + (3 * (self.level_calc() - 10)) + (self.Enrage * int(self.level_calc() / 2))

    def get_def(self):
        return super(Barbarian, self).get_def() + (1 * (self.level_calc() - 10))

    def get_dex(self):
        return super(Barbarian, self).get_dex() + (5 * (self.level_calc() - 10))

    def get_eva(self):
        return super(Barbarian, self).get_eva() + (2 * (self.level_calc() - 10))

    def get_max_stm(self):
        return super(Barbarian, self).get_max_stm()

    def get_max_hp(self):
        return super(Barbarian, self).get_max_hp() + (0 * (self.level_calc() - 10))

    def cleanup(self):
        self.Enrage = 0

    def attack(self):
        crit_count = 0
        damage = self.get_dmg()
        Crit = Parent.GetRandom(0,100)
        if Crit < self.get_crit():
            crit_count = 1
            self.Enrage += 1
            damage += damage - int(damage * 0.5)
        return (damage, crit_count)

#-----------------------------------------Knight (10)-----------------------------------------
class Knight(Warrior):
    def __init__(self, userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash):
        super(Knight, self).__init__(userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash)
        self.Class = "Knight"
        self.Times_hit = 0

    def get_dmg(self):
        return super(Knight, self).get_dmg() + (1 * (self.level_calc() - 10))

    def get_def(self):
        return super(Knight, self).get_def() + (5 * (self.level_calc() - 10))

    def get_dex(self):
        return super(Knight, self).get_dex() + (3 * (self.level_calc() - 10))

    def get_eva(self):
        return super(Knight, self).get_eva() + (3 * (self.level_calc() - 10))

    def get_max_stm(self):
        return super(Knight, self).get_max_stm()

    def get_max_hp(self):
        return super(Knight, self).get_max_hp() + (5 * (self.level_calc() - 10))

    def defend(self, dmg):
        Dodge = Parent.GetRandom(0,100)
        if Dodge < self.get_dodge():
            return 0
        # Count the hit taken for counter attack
        self.Times_hit += 1
        DmgTaken = 0
        Def = self.get_def()
        MaxDmgBlock = int(dmg * 0.9)
        if Def > MaxDmgBlock:
            DmgTaken = dmg - MaxDmgBlock
        else:
            DmgTaken = dmg - Def
        return DmgTaken

    def cleanup(self):
        self.Times_hit = 0

    def standby_pure(self):
        damage = self.Times_hit * 5
        self.Times_hit = 0
        return damage

#-----------------------------------------Scout (1)------------------------------------
class Scout(Player):
    def __init__(self, userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash):
        super(Scout, self).__init__(userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash)
        self.Class = "Scout"
        self.DungeonBonus = 20

    def get_dmg(self):
        mult = self.level_calc()
        if mult > 10:
            mult = 10
        return super(Scout, self).get_dmg() + (3 * mult)

    def get_def(self):
        mult = self.level_calc()
        if mult > 10:
            mult = 10
        return super(Scout, self).get_def() + (0 * mult)

    def get_dex(self):
        mult = self.level_calc()
        if mult > 10:
            mult = 10
        return super(Scout, self).get_dex() + (2 * mult)

    def get_eva(self):
        mult = self.level_calc()
        if mult > 10:
            mult = 10
        return super(Scout, self).get_eva() + (1 * mult)

    def get_max_stm(self):
        return super(Scout, self).get_max_stm()

    def get_max_hp(self):
        mult = self.level_calc()
        if mult > 10:
            mult = 10
        return super(Scout, self).get_max_hp() + (0 * mult)

#-----------------------------------------Assassin (20)-----------------------------------------
class Assassin(Scout):
    def __init__(self, userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash):
        super(Assassin, self).__init__(userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash)
        self.Class = "Assassin"

    def get_dmg(self):
        return super(Assassin, self).get_dmg() + (4 * (self.level_calc() - 10))

    def get_def(self):
        return super(Assassin, self).get_def() + (1 * (self.level_calc() - 10))

    def get_dex(self):
        return super(Assassin, self).get_dex() + (3 * (self.level_calc() - 10))

    def get_eva(self):
        return super(Assassin, self).get_eva() + (2 * (self.level_calc() - 10))

    def get_max_stm(self):
        return super(Assassin, self).get_max_stm()

    def get_max_hp(self):
        return super(Assassin, self).get_max_hp() + (1 * (self.level_calc() - 10))

    def attack(self):
        crit_count = 0
        damage = self.get_dmg()
        Crit = Parent.GetRandom(0,100)
        if Crit < self.get_crit():
            crit_count = 1
            damage += damage
        return (damage, crit_count)

#-----------------------------------------Archer (10)-----------------------------------------
class Archer(Scout):
    def __init__(self, userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash):
        super(Archer, self).__init__(userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash)
        self.Class = "Archer"

    def get_dmg(self):
        return super(Archer, self).get_dmg() + (5 * (self.level_calc() - 10))

    def get_def(self):
        return super(Archer, self).get_def() + (2 * (self.level_calc() - 10))

    def get_dex(self):
        return super(Archer, self).get_dex() + (3 * (self.level_calc() - 10))

    def get_eva(self):
        return super(Archer, self).get_eva() + (1 * (self.level_calc() - 10))

    def get_max_stm(self):
        return super(Archer, self).get_max_stm()

    def get_max_hp(self):
        return super(Archer, self).get_max_hp() + (0 * (self.level_calc() - 10))

    #multi shot
    def standby_attack(self, boss_def):
        MultiShot = Parent.GetRandom(1,5)
        crit_count = 0
        if MultiShot == 1:
            (attack, crit_count) = self.attack()
            MaxDmgBlock = int(attack * 0.75)
            if boss_def > MaxDmgBlock:
                dmg = attack - MaxDmgBlock
            else:
                dmg = attack - boss_def
            return (dmg, crit_count)
        return (0, crit_count)

#-----------------------------------------Mage (1)-----------------------------------------
class Mage(Player):
    def __init__(self, userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash):
        super(Mage, self).__init__(userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash)
        self.Class = "Mage"
        self.LootBonus = 2

    def get_dmg(self):
        mult = self.level_calc()
        if mult > 10:
            mult = 10
        return super(Mage, self).get_dmg() + (3 * mult)

    def get_def(self):
        mult = self.level_calc()
        if mult > 10:
            mult = 10
        return super(Mage, self).get_def() + (0 * mult)

    def get_dex(self):
        mult = self.level_calc()
        if mult > 10:
            mult = 10
        return super(Mage, self).get_dex() + (3 * mult)

    def get_eva(self):
        mult = self.level_calc()
        if mult > 10:
            mult = 10
        return super(Mage, self).get_eva() + (0 * mult)

    def get_max_stm(self):
        return super(Mage, self).get_max_stm()

    def get_max_hp(self):
        mult = self.level_calc()
        if mult > 10:
            mult = 10
        return super(Mage, self).get_max_hp() + (0 * mult)

#-----------------------------------------Wizard (10)-----------------------------------------
class Wizard(Mage):
    def __init__(self, userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash):
        super(Wizard, self).__init__(userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash)
        self.Class = "Wizard"

    def get_dmg(self):
        return super(Wizard, self).get_dmg() + (4 * (self.level_calc() - 10))

    def get_def(self):
        return super(Wizard, self).get_def() + (1 * (self.level_calc() - 10))

    def get_dex(self):
        return super(Wizard, self).get_dex() + (8 * (self.level_calc() - 10))

    def get_eva(self):
        return super(Wizard, self).get_eva() + (1 * (self.level_calc() - 10))

    def get_max_stm(self):
        return super(Wizard, self).get_max_stm()

    def get_max_hp(self):
        return super(Wizard, self).get_max_hp() + (1 * (self.level_calc() - 10))

#-----------------------------------------Illusionist (10)-----------------------------------------
class Illusionist(Mage):
    def __init__(self, userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash):
        super(Illusionist, self).__init__(userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash)
        self.Class = "Illusionist"
        self.Clones = 0

    def get_dmg(self):
        return super(Illusionist, self).get_dmg() + (2 * (self.level_calc() - 10)) + (self.Clones * 10)

    def get_def(self):
        return super(Illusionist, self).get_def() + (3 * (self.level_calc() - 10))

    def get_dex(self):
        return super(Illusionist, self).get_dex() + (0 * (self.level_calc() - 10))

    def get_eva(self):
        return super(Illusionist, self).get_eva() + (0 * (self.level_calc() - 10))

    def get_max_stm(self):
        return super(Illusionist, self).get_max_stm()

    def get_max_hp(self):
        return super(Illusionist, self).get_max_hp() + (2 * (self.level_calc() - 10))

    def standby(self, Allies):
        self.Clones += 1
        return 0

    def defend(self, dmg):
        Dodge = Parent.GetRandom(0,100)
        if Dodge < self.get_dodge():
            return 0
        # Reset clones because the player got hit
        self.Clones = 0
        DmgTaken = 0
        Def = self.get_def()
        MaxDmgBlock = int(dmg * 0.75)
        if Def > MaxDmgBlock:
            DmgTaken = dmg - MaxDmgBlock
        else:
            DmgTaken = dmg - Def
        return DmgTaken

    def cleanup(self):
        self.Clones = 0

#-----------------------------------------Cleric (1)-----------------------------------------
class Cleric(Player):
    def __init__(self, userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash):
        super(Cleric, self).__init__(userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash)
        self.Class = "Cleric"
        self.RegenAura = 1

    def get_dmg(self):
        mult = self.level_calc()
        if mult > 10:
            mult = 10
        return super(Cleric, self).get_dmg() + (1 * mult)

    def get_def(self):
        mult = self.level_calc()
        if mult > 10:
            mult = 10
        return super(Cleric, self).get_def() + (2 * mult)

    def get_dex(self):
        mult = self.level_calc()
        if mult > 10:
            mult = 10
        return super(Cleric, self).get_dex() + (2 * mult)

    def get_eva(self):
        mult = self.level_calc()
        if mult > 10:
            mult = 10
        return super(Cleric, self).get_eva() + (0 * mult)

    def get_max_stm(self):
        return super(Cleric, self).get_max_stm()

    def get_max_hp(self):
        mult = self.level_calc()
        if mult > 10:
            mult = 10
        return super(Cleric, self).get_max_hp() + (1 * mult)

    def standby(self, Allies):
        heal_done = 0
        Len = len(Allies.PList)
        if Len > 0:
            i = 0
            while i < Len:
                h = Allies.PList[i].add_hp(self.RegenAura)
                heal_done += h
                i += 1
        return heal_done

#-----------------------------------------Priest (10)-----------------------------------------
class Priest(Cleric):
    def __init__(self, userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash):
        super(Priest, self).__init__(userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash)
        self.Class = "Priest"
        self.Heal = 5

    def get_dmg(self):
        return super(Priest, self).get_dmg() + (3 * (self.level_calc() - 10))

    def get_def(self):
        return super(Priest, self).get_def() + (1 * (self.level_calc() - 10))

    def get_dex(self):
        return super(Priest, self).get_dex() + (2 * (self.level_calc() - 10))

    def get_eva(self):
        return super(Priest, self).get_eva() + (1 * (self.level_calc() - 10))

    def get_max_stm(self):
        return super(Priest, self).get_max_stm()

    def get_max_hp(self):
        return super(Priest, self).get_max_hp() + (2 * (self.level_calc() - 10))

    def standby(self, Allies):
        heal_done = 0
        Len = len(Allies.PList)
        if Len > 0:
            WoundI = 0
            i = 0
            while i < Len:
                h = Allies.PList[i].add_hp(self.RegenAura)
                heal_done += h
                if Allies.PList[i].Hp < Allies.PList[WoundI].Hp:
                    WoundI = i
                i += 1
            AggroLen = len(Allies.AggroList)
            Aggro = Allies.get_player_aggro(self.UserId)
            HealValue = int((Aggro * Len / AggroLen) + 0.9999)
            h = Allies.PList[WoundI].add_hp(self.Heal * HealValue)
            heal_done += h
        return heal_done

#-----------------------------------------Paladin (20)-----------------------------------------
class Paladin(Cleric):
    def __init__(self, userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash):
        super(Paladin, self).__init__(userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash)
        self.Class = "Paladin"
        self.Crit = False

    def get_dmg(self):
        return super(Paladin, self).get_dmg() + (2 * (self.level_calc() - 10))

    def get_def(self):
        return super(Paladin, self).get_def() + (5 * (self.level_calc() - 10))

    def get_dex(self):
        return super(Paladin, self).get_dex() + (0 * (self.level_calc() - 10))

    def get_eva(self):
        return super(Paladin, self).get_eva() + (3 * (self.level_calc() - 10))

    def get_max_stm(self):
        return super(Paladin, self).get_max_stm()

    def get_max_hp(self):
        return super(Paladin, self).get_max_hp() + (4 * (self.level_calc() - 10))

    def defend(self, dmg):
        Dodge = Parent.GetRandom(0,100)
        if Dodge < self.get_dodge():
            return 0
        DmgTaken = 0
        Def = self.get_def()
        MaxDmgBlock = int(dmg * 0.85)
        if Def > MaxDmgBlock:
            DmgTaken = dmg - MaxDmgBlock
        else:
            DmgTaken = dmg - Def
        return DmgTaken

    def standby(self, Allies):
        heal_done = 0
        if self.Crit:
            heal_done += self.add_hp(10)
        Len = len(Allies.PList)
        if Len > 0:
            i = 0
            while i < Len:
                h = Allies.PList[i].add_hp(self.RegenAura)
                heal_done += h
                i += 1
        return heal_done

    def cleanup(self):
        self.Crit = False

    def attack(self):
        self.Crit = False
        crit_count = 0
        damage = self.get_dmg()
        Crit = Parent.GetRandom(0,100)
        if Crit < self.get_crit():
            crit_count = 1
            damage += damage - int(damage * 0.5)
            self.Crit = True
        return (damage, crit_count)

#-----------------------------------------Shaman (1)-----------------------------------------
class Shaman(Player):
    def __init__(self, userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash):
        super(Shaman, self).__init__(userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash)
        self.Class = "Shaman"
        self.Allies = 1

    def get_dmg(self):
        mult = self.level_calc()
        if mult > 10:
            mult = 10
        return super(Shaman, self).get_dmg() + (2 * mult)

    def get_def(self):
        mult = self.level_calc()
        if mult > 10:
            mult = 10
        return super(Shaman, self).get_def() + (0 * mult)

    def get_dex(self):
        mult = self.level_calc()
        if mult > 10:
            mult = 10
        return super(Shaman, self).get_dex() + (2 * mult)

    def get_eva(self):
        mult = self.level_calc()
        if mult > 10:
            mult = 10
        return super(Shaman, self).get_eva() + (2 * mult)

    def get_max_stm(self):
        return super(Shaman, self).get_max_stm()

    def get_max_hp(self):
        mult = self.level_calc()
        if mult > 10:
            mult = 10
        return super(Shaman, self).get_max_hp() + (0 * mult)

    def standby(self, Allies):
        heal_done = 0
        Len = len(Allies.PList)
        self.Allies = Len
        if Len > 0:
            I = Parent.GetRandom(0, Len)
            heal_done = Allies.PList[I].add_hp(self.Allies)
        return heal_done

    def cleanup(self):
        self.Allies = 1

#-----------------------------------------Warlock (10)-----------------------------------------
class Warlock(Shaman):
    def __init__(self, userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash):
        super(Warlock, self).__init__(userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash)
        self.Class = "Warlock"

    def get_dmg(self):
        return super(Warlock, self).get_dmg() + (3 * (self.level_calc() - 10))

    def get_def(self):
        return super(Warlock, self).get_def() + (2 * (self.level_calc() - 10))

    def get_dex(self):
        return super(Warlock, self).get_dex() + (3 * (self.level_calc() - 10))

    def get_eva(self):
        return super(Warlock, self).get_eva() + (3 * (self.level_calc() - 10))

    def get_max_stm(self):
        return super(Warlock, self).get_max_stm()

    def get_max_hp(self):
        return super(Warlock, self).get_max_hp() + (2 * (self.level_calc() - 10))

    def attack(self):
        return (0, 0)

    def standby_attack(self, boss_def):
        dmg = self.Allies
        DmgSum = 0
        crit_count = 0
        attacks = int(self.get_dmg() / 25 + 0.9999)
        while attacks > 0:
            Crit = Parent.GetRandom(0,100)
            if Crit < self.get_crit():
                DmgSum += (dmg * 2) - int(dmg * 0.5)
                crit_count += 1
            else:
                DmgSum += dmg
            attacks -= 1
        return (DmgSum, crit_count)

#-----------------------------------------Druid (1)-----------------------------------------
class Druid(Player):
    def __init__(self, userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash):
        super(Druid, self).__init__(userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash)
        self.Class = "Druid"

    def get_dmg(self):
        mult = self.level_calc()
        if mult > 10:
            mult = 10
        return super(Druid, self).get_dmg() + (0 * mult)

    def get_def(self):
        mult = self.level_calc()
        if mult > 10:
            mult = 10
        return super(Druid, self).get_def() + (3 * mult)

    def get_dex(self):
        mult = self.level_calc()
        if mult > 10:
            mult = 10
        return super(Druid, self).get_dex() + (0 * mult)

    def get_eva(self):
        mult = self.level_calc()
        if mult > 10:
            mult = 10
        return super(Druid, self).get_eva() + (0 * mult)

    def get_max_stm(self):
        return super(Druid, self).get_max_stm()

    def get_max_hp(self):
        mult = self.level_calc()
        if mult > 10:
            mult = 10
        return super(Druid, self).get_max_hp() + (3 * mult)

    def defend(self, dmg):
        Dodge = Parent.GetRandom(0,100)
        if Dodge < self.get_dodge():
            return 0
        DmgTaken = 0
        Def = self.get_def()
        MaxDmgBlock = int(dmg * 0.80)
        if Def > MaxDmgBlock:
            DmgTaken = dmg - MaxDmgBlock
        else:
            DmgTaken = dmg - Def
        return DmgTaken

#-----------------------------------------Naturalist (10)-----------------------------------------
class Naturalist(Druid):
    def __init__(self, userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash):
        super(Naturalist, self).__init__(userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash)
        self.Class = "Naturalist"

    def get_dmg(self):
        return super(Naturalist, self).get_dmg() + (3 * (self.level_calc() - 10))

    def get_def(self):
        return super(Naturalist, self).get_def() + (0 * (self.level_calc() - 10))

    def get_dex(self):
        return super(Naturalist, self).get_dex() + (3 * (self.level_calc() - 10))

    def get_eva(self):
        return super(Naturalist, self).get_eva() + (3 * (self.level_calc() - 10))

    def get_max_stm(self):
        return super(Naturalist, self).get_max_stm()

    def get_max_hp(self):
        return super(Naturalist, self).get_max_hp() + (0 * (self.level_calc() - 10))

    def standby(self, Allies):
        heal_done = 0
        Len = len(Allies.PList)
        if Len > 0:
            Heals = 10
            while Heals > 0 :
                I = Parent.GetRandom(0, Len)
                Heal = 2
                if Allies.PList[I] == self:
                    Heal = 1
                h = Allies.PList[I].add_hp(Heal)
                heal_done += h
                Heals = Heals - 1
        return heal_done

#-----------------------------------------Shapeshifter (10)-----------------------------------------
class Shapeshifter(Druid):
    def __init__(self, userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash):
        super(Shapeshifter, self).__init__(userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash)
        self.Class = "Shapeshifter"

    def get_dmg(self):
        return super(Shapeshifter, self).get_dmg() + (4 * (self.level_calc() - 10))

    def get_def(self):
        return super(Shapeshifter, self).get_def() + (5 * (self.level_calc() - 10))

    def get_dex(self):
        return super(Shapeshifter, self).get_dex() + (4 * (self.level_calc() - 10))

    def get_eva(self):
        return super(Shapeshifter, self).get_eva() + (4 * (self.level_calc() - 10))

    def get_max_stm(self):
        return super(Shapeshifter, self).get_max_stm()

    def get_max_hp(self):
        return super(Shapeshifter, self).get_max_hp() + (7 * (self.level_calc() - 10))

#-----------------------------------------Elementalist (1)-----------------------------------------
class Elementalist(Player):
    def __init__(self, userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash):
        super(Elementalist, self).__init__(userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash)
        self.Class = "Elementalist"

    def get_dmg(self):
        mult = self.level_calc()
        if mult > 10:
            mult = 10
        return super(Elementalist, self).get_dmg() + (2 * mult)

    def get_def(self):
        mult = self.level_calc()
        if mult > 10:
            mult = 10
        return super(Elementalist, self).get_def() + (2 * mult)

    def get_dex(self):
        mult = self.level_calc()
        if mult > 10:
            mult = 10
        return super(Elementalist, self).get_dex() + (2 * mult)

    def get_eva(self):
        mult = self.level_calc()
        if mult > 10:
            mult = 10
        return super(Elementalist, self).get_eva() + (2 * mult)

    def get_max_stm(self):
        return super(Elementalist, self).get_max_stm()

    def get_max_hp(self):
        mult = self.level_calc()
        if mult > 10:
            mult = 10
        return super(Elementalist, self).get_max_hp() + (0 * mult)

#-----------------------------------------Geomancer (10)-----------------------------------------
class Geomancer(Elementalist):
    def __init__(self, userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash):
        super(Geomancer, self).__init__(userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash)
        self.Class = "Geomancer"

    def get_dmg(self):
        return super(Geomancer, self).get_dmg() + (2 * (self.level_calc() - 10))

    def get_def(self):
        return super(Geomancer, self).get_def() + (8 * (self.level_calc() - 10))

    def get_dex(self):
        return super(Geomancer, self).get_dex() + (0 * (self.level_calc() - 10))

    def get_eva(self):
        return super(Geomancer, self).get_eva() + (0 * (self.level_calc() - 10))

    def get_max_stm(self):
        return super(Geomancer, self).get_max_stm()

    def get_max_hp(self):
        return super(Geomancer, self).get_max_hp() + (5 * (self.level_calc() - 10))

    def defend(self, dmg):
        Earth_shield = Parent.GetRandom(1,5)
        if Earth_shield == 1:
            return 0
        Dodge = Parent.GetRandom(0,100)
        if Dodge < self.get_dodge():
            return 0
        DmgTaken = 0
        Def = self.get_def()
        MaxDmgBlock = int(dmg * 0.80)
        if Def > MaxDmgBlock:
            DmgTaken = dmg - MaxDmgBlock
        else:
            DmgTaken = dmg - Def
        return DmgTaken

#-----------------------------------------Hydromancer (10)-----------------------------------------
class Hydromancer(Elementalist):
    def __init__(self, userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash):
        super(Hydromancer, self).__init__(userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash)
        self.Class = "Hydromancer"

    def get_dmg(self):
        return super(Hydromancer, self).get_dmg() + (1 * (self.level_calc() - 10))

    def get_def(self):
        return super(Hydromancer, self).get_def() + (3 * (self.level_calc() - 10))

    def get_dex(self):
        return super(Hydromancer, self).get_dex() + (1 * (self.level_calc() - 10))

    def get_eva(self):
        return super(Hydromancer, self).get_eva() + (3 * (self.level_calc() - 10))

    def get_max_stm(self):
        return super(Hydromancer, self).get_max_stm()

    def get_max_hp(self):
        return super(Hydromancer, self).get_max_hp() + (0 * (self.level_calc() - 10))

    def standby(self, Allies):
        heal_done = 0
        heal_roll = Parent.GetRandom(1,5)
        Len = len(Allies.PList)
        if Len > 0:
            i = 0
            while i < Len:
                h = Allies.PList[i].add_hp(heal_roll)
                heal_done += h
                i += 1
        return heal_done

#-----------------------------------------Aeromancer (10)-----------------------------------------
class Aeromancer(Elementalist):
    def __init__(self, userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash):
        super(Aeromancer, self).__init__(userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash)
        self.Class = "Aeromancer"

    def get_dmg(self):
        return super(Aeromancer, self).get_dmg() + (1 * (self.level_calc() - 10))

    def get_def(self):
        return super(Aeromancer, self).get_def() + (2 * (self.level_calc() - 10))

    def get_dex(self):
        return super(Aeromancer, self).get_dex() + (0 * (self.level_calc() - 10))

    def get_eva(self):
        return super(Aeromancer, self).get_eva() + (3 * (self.level_calc() - 10))

    def get_max_stm(self):
        return super(Aeromancer, self).get_max_stm()

    def get_max_hp(self):
        return super(Aeromancer, self).get_max_hp() + (1 * (self.level_calc() - 10))

    def attack(self):
        return (0, 0)

    def standby_attack(self, boss_def):
        attack = True
        DmgSum = 0
        crit_count = 0
        while attack:
            attack = False
            # Damage against defense
            damage = self.get_dmg()
            MaxDmgBlock = int(damage * 0.75)
            if boss_def > MaxDmgBlock:
                dmg = damage - MaxDmgBlock
            else:
                dmg = damage - boss_def
            DmgSum += dmg
            # Rolls for extra attack
            Crit = Parent.GetRandom(0,100)
            if Crit < self.get_crit():
                crit_count += 1
                attack = True
        # Returns damage sum
        return (DmgSum, crit_count)

#-----------------------------------------Pyromancer (10)-----------------------------------------
class Pyromancer(Elementalist):
    def __init__(self, userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash):
        super(Pyromancer, self).__init__(userId, pname, pexp, php, pstm, pdmg, pdef, pdex, peva, kc, rc, titles, currency, consumable, stash)
        self.Class = "Pyromancer"

    def get_dmg(self):
        return super(Pyromancer, self).get_dmg() + (8 * (self.level_calc() - 10))

    def get_def(self):
        return super(Pyromancer, self).get_def() + (1 * (self.level_calc() - 10))

    def get_dex(self):
        return super(Pyromancer, self).get_dex() + (5 * (self.level_calc() - 10))

    def get_eva(self):
        return super(Pyromancer, self).get_eva() + (1 * (self.level_calc() - 10))

    def get_max_stm(self):
        return super(Pyromancer, self).get_max_stm()

    def get_max_hp(self):
        return super(Pyromancer, self).get_max_hp() + (0 * (self.level_calc() - 10))

#-----------------------------------------Class change method-----------------------------------------
def class_change(player, nClass):
    message = "invalid class change"
    auxplayer = None
    if (nClass == "warrior") and (player.Class == "Adventurer") and (player.level_calc() >= 1):
        auxplayer = Warrior(player.UserId, player.Name, player.Exp, player.Hp, player.Stm, player.Dmg, player.Def, player.Dex, player.Eva, player.KillCount, player.ResetCount, player.Titles, player.Currency, player.Consumable, player.Stash)
        message = "You are now a " + auxplayer.Class
    elif nClass == "barbarian" and (player.Class == "Warrior") and (player.level_calc() >= 10):
        auxplayer = Barbarian(player.UserId, player.Name, player.Exp, player.Hp, player.Stm, player.Dmg, player.Def, player.Dex, player.Eva, player.KillCount, player.ResetCount, player.Titles, player.Currency, player.Consumable, player.Stash)
        message = "You are now a " + auxplayer.Class
    elif nClass == "knight" and (player.Class == "Warrior") and (player.level_calc() >= 10):
        auxplayer = Knight(player.UserId, player.Name, player.Exp, player.Hp, player.Stm, player.Dmg, player.Def, player.Dex, player.Eva, player.KillCount, player.ResetCount, player.Titles, player.Currency, player.Consumable, player.Stash)
        message = "You are now a " + auxplayer.Class
#-----------------------------------------------------------------------------
    elif nClass == "scout" and (player.Class == "Adventurer") and (player.level_calc() >= 1):
        auxplayer = Scout(player.UserId, player.Name, player.Exp, player.Hp, player.Stm, player.Dmg, player.Def, player.Dex, player.Eva, player.KillCount, player.ResetCount, player.Titles, player.Currency, player.Consumable, player.Stash)
        message = "You are now a " + auxplayer.Class
    elif nClass == "assassin" and (player.Class == "Scout") and (player.level_calc() >= 10):
        auxplayer = Assassin(player.UserId, player.Name, player.Exp, player.Hp, player.Stm, player.Dmg, player.Def, player.Dex, player.Eva, player.KillCount, player.ResetCount, player.Titles, player.Currency, player.Consumable, player.Stash)
        message = "You are now an " + auxplayer.Class
    elif nClass == "archer" and (player.Class == "Scout") and (player.level_calc() >= 10):
        auxplayer = Archer(player.UserId, player.Name, player.Exp, player.Hp, player.Stm, player.Dmg, player.Def, player.Dex, player.Eva, player.KillCount, player.ResetCount, player.Titles, player.Currency, player.Consumable, player.Stash)
        message = "You are now an " + auxplayer.Class
#-----------------------------------------------------------------------------
    elif nClass == "mage" and (player.Class == "Adventurer") and (player.level_calc() >= 1):
        auxplayer = Mage(player.UserId, player.Name, player.Exp, player.Hp, player.Stm, player.Dmg, player.Def, player.Dex, player.Eva, player.KillCount, player.ResetCount, player.Titles, player.Currency, player.Consumable, player.Stash)
        message = "You are now a " + auxplayer.Class
    elif nClass == "wizard" and (player.Class == "Mage") and (player.level_calc() >= 10):
        auxplayer = Wizard(player.UserId, player.Name, player.Exp, player.Hp, player.Stm, player.Dmg, player.Def, player.Dex, player.Eva, player.KillCount, player.ResetCount, player.Titles, player.Currency, player.Consumable, player.Stash)
        message = "You are now a " + auxplayer.Class
    elif nClass == "illusionist" and (player.Class == "Mage") and (player.level_calc() >= 10):
        auxplayer = Illusionist(player.UserId, player.Name, player.Exp, player.Hp, player.Stm, player.Dmg, player.Def, player.Dex, player.Eva, player.KillCount, player.ResetCount, player.Titles, player.Currency, player.Consumable, player.Stash)
        message = "You are now an " + auxplayer.Class
#-----------------------------------------------------------------------------
    elif nClass == "cleric" and (player.Class == "Adventurer") and (player.level_calc() >= 1):
        auxplayer = Cleric(player.UserId, player.Name, player.Exp, player.Hp, player.Stm, player.Dmg, player.Def, player.Dex, player.Eva, player.KillCount, player.ResetCount, player.Titles, player.Currency, player.Consumable, player.Stash)
        message = "You are now a " + auxplayer.Class
    elif nClass == "priest" and (player.Class == "Cleric") and (player.level_calc() >= 10):
        auxplayer = Priest(player.UserId, player.Name, player.Exp, player.Hp, player.Stm, player.Dmg, player.Def, player.Dex, player.Eva, player.KillCount, player.ResetCount, player.Titles, player.Currency, player.Consumable, player.Stash)
        message = "You are now a " + auxplayer.Class
    elif nClass == "paladin" and (player.Class == "Cleric") and (player.level_calc() >= 10):
        auxplayer = Paladin(player.UserId, player.Name, player.Exp, player.Hp, player.Stm, player.Dmg, player.Def, player.Dex, player.Eva, player.KillCount, player.ResetCount, player.Titles, player.Currency, player.Consumable, player.Stash)
        message = "You are now a " + auxplayer.Class
#-----------------------------------------------------------------------------
    elif nClass == "shaman" and (player.Class == "Adventurer") and (player.level_calc() >= 1):
        auxplayer = Shaman(player.UserId, player.Name, player.Exp, player.Hp, player.Stm, player.Dmg, player.Def, player.Dex, player.Eva, player.KillCount, player.ResetCount, player.Titles, player.Currency, player.Consumable, player.Stash)
        message = "You are now a " + auxplayer.Class
    elif nClass == "warlock" and (player.Class == "Shaman") and (player.level_calc() >= 10):
        auxplayer = Warlock(player.UserId, player.Name, player.Exp, player.Hp, player.Stm, player.Dmg, player.Def, player.Dex, player.Eva, player.KillCount, player.ResetCount, player.Titles, player.Currency, player.Consumable, player.Stash)
        message = "You are now a " + auxplayer.Class
#-----------------------------------------------------------------------------
    elif nClass == "druid" and (player.Class == "Adventurer") and (player.level_calc() >= 1) and (player.ResetCount >= 1):
        auxplayer = Druid(player.UserId, player.Name, player.Exp, player.Hp, player.Stm, player.Dmg, player.Def, player.Dex, player.Eva, player.KillCount, player.ResetCount, player.Titles, player.Currency, player.Consumable, player.Stash)
        message = "You are now a " + auxplayer.Class
    elif nClass == "naturalist" and (player.Class == "Druid") and (player.level_calc() >= 10):
        auxplayer = Naturalist(player.UserId, player.Name, player.Exp, player.Hp, player.Stm, player.Dmg, player.Def, player.Dex, player.Eva, player.KillCount, player.ResetCount, player.Titles, player.Currency, player.Consumable, player.Stash)
        message = "You are now a " + auxplayer.Class
    elif nClass == "shapeshifter" and (player.Class == "Druid") and (player.level_calc() >= 10):
        auxplayer = Shapeshifter(player.UserId, player.Name, player.Exp, player.Hp, player.Stm, player.Dmg, player.Def, player.Dex, player.Eva, player.KillCount, player.ResetCount, player.Titles, player.Currency, player.Consumable, player.Stash)
        message = "You are now a " + auxplayer.Class
#-----------------------------------------------------------------------------
    elif nClass == "elementalist" and (player.Class == "Adventurer") and (player.level_calc() >= 1) and (player.ResetCount >= 2):
        auxplayer = Elementalist(player.UserId, player.Name, player.Exp, player.Hp, player.Stm, player.Dmg, player.Def, player.Dex, player.Eva, player.KillCount, player.ResetCount, player.Titles, player.Currency, player.Consumable, player.Stash)
        message = "You are now an " + auxplayer.Class
    elif nClass == "geomancer" and (player.Class == "Elementalist") and (player.level_calc() >= 10):
        auxplayer = Geomancer(player.UserId, player.Name, player.Exp, player.Hp, player.Stm, player.Dmg, player.Def, player.Dex, player.Eva, player.KillCount, player.ResetCount, player.Titles, player.Currency, player.Consumable, player.Stash)
        message = "You are now a " + auxplayer.Class
    elif nClass == "hydromancer" and (player.Class == "Elementalist") and (player.level_calc() >= 10):
        auxplayer = Hydromancer(player.UserId, player.Name, player.Exp, player.Hp, player.Stm, player.Dmg, player.Def, player.Dex, player.Eva, player.KillCount, player.ResetCount, player.Titles, player.Currency, player.Consumable, player.Stash)
        message = "You are now a " + auxplayer.Class
    elif nClass == "aeromancer" and (player.Class == "Elementalist") and (player.level_calc() >= 10):
        auxplayer = Aeromancer(player.UserId, player.Name, player.Exp, player.Hp, player.Stm, player.Dmg, player.Def, player.Dex, player.Eva, player.KillCount, player.ResetCount, player.Titles, player.Currency, player.Consumable, player.Stash)
        message = "You are now an " + auxplayer.Class
    elif nClass == "pyromancer" and (player.Class == "Elementalist") and (player.level_calc() >= 10):
        auxplayer = Pyromancer(player.UserId, player.Name, player.Exp, player.Hp, player.Stm, player.Dmg, player.Def, player.Dex, player.Eva, player.KillCount, player.ResetCount, player.Titles, player.Currency, player.Consumable, player.Stash)
        message = "You are now a " + auxplayer.Class
#-----------------------------------------------------------------------------
    if MySet.Mode == 0:
        Parent.SendStreamWhisper(player.UserId, message)
    elif MySet.Mode == 1:
        Parent.SendDiscordDM(player.UserId, message)
    return auxplayer

class MvpPoints:
    def __init__(self, p_id):
        self.UserId = p_id
        self.Dmg_done = 0
        self.Heal_done = 0
        self.Hits_taken = 0
        self.Dmg_mit = 0
        self.Dmg_taken = 0
        self.Dodges = 0
        self.Crits = 0
        self.Rounds = 0

    def add_dmg_done(self, dmg):
        self.Dmg_done += dmg

    def add_heal(self, heal):
        self.Heal_done += heal

    def add_hit(self):
        self.Hits_taken += 1

    def add_dmg_mit(self, dmg):
        self.Dmg_mit += dmg

    def add_dmg_taken(self, dmg):
        self.Dmg_taken += dmg

    def add_dodge(self):
        self.Dodges += 1

    def add_crit(self, crit_count):
        self.Crits += crit_count

    def add_round(self):
        self.Rounds += 1

    def send_summary(self):
        summary = 'Damage done: ' + str(self.Dmg_done) + ', critical hits: ' + str(self.Crits) + ', healing done: ' + str(self.Heal_done) + ', hits taken: ' + str(self.Hits_taken) + ', damage mitigated: ' + str(self.Dmg_mit) + ', damage taken: ' + str(self.Dmg_taken) + ', attacks dodged: ' + str(self.Dodges) + ', rounds: ' + str(self.Rounds)
        if MySet.Mode == 0:
            Parent.SendStreamWhisper(self.UserId, summary)
        elif MySet.Mode == 1:
            Parent.SendDiscordDM(self.UserId, summary)

class PlayerList:
    def __init__(self):
        self.PList = []
        self.MvpList = []
        self.AggroList = []

    def lenght(self):
        return len(self.PList)

    def add(self, p, aggro):
        i = 0
        self.PList.append(p)
        self.MvpList.append(MvpPoints(p.UserId))
        while i < aggro:
            self.AggroList.append(p.UserId)
            i+= 1

    def save_listed_players(self, c):
        Len = len(self.PList)
        if Len > 0:
            i = 0
            while i < Len:
                auxplayer = self.PList[i]
                save_player(c, auxplayer)
                i+= 1

    def remove_by_id(self, p_id):
        Len = len(self.PList)
        if Len > 0:
            i = 0
            while i < Len and (self.PList[i].UserId != p_id):
                if self.PList[i].UserId != p_id:
                    i+= 1
            if i < Len:
                self.PList.pop(i)

    def search_by_id(self, p_id):
        Len = len(self.PList)
        if Len > 0:
            i = 0
            while i < Len and (self.PList[i].UserId != p_id):
                if self.PList[i].UserId != p_id:
                    i+= 1
            if i < Len:
                player = self.PList[i]
                return player
        return None

    def get_target_index(self):
        if len(self.AggroList) > 0:
            p_ind = Parent.GetRandom(0, len(self.AggroList))
            p_id = self.AggroList[p_ind]
            Len = len(self.PList)
            if Len > 0:
                i = 0
                while i < Len and (self.PList[i].UserId != p_id):
                    if self.PList[i].UserId != p_id:
                        i+= 1
                if i < Len:
                    return i
        return None

    def get_player_aggro(self, pid):
        Len = len(self.AggroList)
        acu = 0
        if Len > 0:
            i = 0
            while i < Len:
                if self.AggroList[i] == pid:
                    acu += 1
                i+= 1
        return acu

    def remove_from_aggro(self, pid):
        Len = len(self.AggroList)
        if Len > 0:
            i = 0
            while i < Len:
                if self.AggroList[i] == pid:
                    self.AggroList.pop(i)
                    Len-= 1
                else:
                    i+= 1
        
    def take_dmg(self, c, boss_dmg):
        Len = len(self.PList)
        if Len > 0:
            ati = 0
            while ati < Len:
                i = self.get_target_index()
                if i != None:
                    DmgToTake = int(self.PList[i].defend(boss_dmg))
                    if DmgToTake > 0:
                        self.PList[i].add_hp(-DmgToTake)
                        if DmgToTake > 0:
                            self.MvpList[i].add_dmg_taken(DmgToTake)
                            self.MvpList[i].add_dmg_mit(boss_dmg - DmgToTake)
                            self.MvpList[i].add_hit()
                    else:
                        self.MvpList[i].add_dmg_mit(boss_dmg)
                        self.MvpList[i].add_dodge()
                    if self.PList[i].Hp <= 0:
                        player_ko(c, self.PList[i].UserId)
                        self.remove_from_aggro(self.PList[i].UserId)
                        self.PList.pop(i)
                        self.MvpList.append(self.MvpList[i])
                        self.MvpList.pop(i)
                ati+= 1

    def dmg_boss(self, boss_def):
        Len = len(self.PList)
        if Len > 0:
            DmgSum = 0
            #Standby buff and party effects
            i = 0
            while i < Len:
                self.MvpList[i].add_round()
                h = self.PList[i].standby(self)
                self.MvpList[i].add_heal(h)
                i += 1
            #Standby pure damage
            i = 0
            while i < Len:
                dmg = self.PList[i].standby_pure()
                self.MvpList[i].add_dmg_done(dmg)
                DmgSum += dmg
                i += 1
            #Standby attacks against defense
            i = 0
            while i < Len:
                (dmg, crit_count) = self.PList[i].standby_attack(boss_def)
                self.MvpList[i].add_crit(crit_count)
                self.MvpList[i].add_dmg_done(int(dmg))
                DmgSum += dmg
                i += 1
            #Attacks
            i = 0
            while i < Len:
                (attack, crit_count) = self.PList[i].attack()
                MaxDmgBlock = int(attack * 0.75)
                if boss_def > MaxDmgBlock:
                    dmg = attack - MaxDmgBlock
                else:
                    dmg = attack - boss_def
                self.MvpList[i].add_crit(crit_count)
                self.MvpList[i].add_dmg_done(dmg)
                DmgSum += dmg
                i += 1
            return DmgSum
        return 0

    def round_fatigue(self):
        Len = len(self.PList)
        if Len > 0:
            i = 0
            while i < Len:
                self.PList[i].add_stm(-1)
                i += 1

    def cleanup(self):
        Len = len(self.PList)
        if Len > 0:
            i = 0
            while i < Len:
                self.PList[i].cleanup()
                i += 1

    def gain_exp(self, exp):
        Len = len(self.PList)
        if Len > 0:
            i = 0
            while i < Len:
                self.PList[i].add_exp(exp)
                self.PList[i].add_boss_kill()
                i += 1

    def gain_currency(self, currency):
        Len = len(self.PList)
        if Len > 0:
            i = 0
            while i < Len:
                self.PList[i].add_currency(currency)
                i += 1

    def loot_chance(self, boss_lvl):
        if boss_lvl < 2:
            return
        Len = len(self.PList)
        if Len > 0:
            i = 0
            while i < Len:
                lootRoll = Parent.GetRandom(1,9)
                if lootRoll <= 4:
                    stats_gained = int(boss_lvl / 2)
                    if lootRoll == 1:
                        sGained = self.PList[i].add_dmg(stats_gained)
                        messageLoot = ('You found a fire relic in the remains of the boss! +' + str(sGained) + ' Damage.')
                    elif lootRoll == 2:
                        sGained = self.PList[i].add_def(stats_gained)
                        messageLoot = ('You found an earth relic in the remains of the boss! +' + str(sGained) + ' Defense.')
                    elif lootRoll == 3:
                        sGained = self.PList[i].add_dex(stats_gained)
                        messageLoot = ('You found a thunder relic in the remains of the boss! +' + str(sGained) + ' Dexterity.')
                    elif lootRoll == 4:
                        sGained = self.PList[i].add_eva(stats_gained)
                        messageLoot = ('You found a wind relic in the remains of the boss! +' + str(sGained) + ' Evasion.')
                    if MySet.Mode == 0:
                        Parent.SendStreamWhisper(self.PList[i].UserId, messageLoot)
                    elif MySet.Mode == 1:
                        Parent.SendDiscordDM(self.PList[i].UserId, messageLoot)
                i += 1

    def give_title(self, title):
        Len = len(self.PList)
        if Len > 0:
            i = 0
            while i < Len:
                self.PList[i].gain_title(title)
                i += 1

    def battle_summary(self):
        Len = len(self.MvpList)
        if Len > 0:
            i = 0
            while i < Len:
                self.MvpList[i].send_summary()
                if i < len(self.PList) and self.MvpList[i].UserId == self.PList[i].UserId:
                    if self.MvpList[i].Dmg_done >= 1000:
                        self.PList[i].gain_title("Rampage")
                    if self.MvpList[i].Heal_done >= 500:
                        self.PList[i].gain_title("Healbot")
                    if self.MvpList[i].Hits_taken >= 20:
                        self.PList[i].gain_title("Punching bag")
                    if self.MvpList[i].Dmg_mit >= 3000:
                        self.PList[i].gain_title("Indestructible")
                    if self.MvpList[i].Dmg_taken >= 300:
                        self.PList[i].gain_title("Meatshield")
                    if self.MvpList[i].Dodges >= 15:
                        self.PList[i].gain_title("Untouchable")
                    if self.MvpList[i].Crits >= 30:
                        self.PList[i].gain_title("Aimbot")
                i += 1

class Boss:
    def __init__(self, bname, blvl, bexp, bhp, bdmg, bdef, title, png):
        self.Name = bname
        self.Level = blvl
        self.Exp = bexp
        self.Hp = bhp
        self.Dmg = bdmg
        self.Def = bdef
        self.Title = title
        self.Png = png

    def soloMode_scaling(self, scaleFactor):
        self.Hp = self.Hp * scaleFactor
        self.Dmg = int(self.Dmg * scaleFactor / 2)
        self.Def = int(self.Def * scaleFactor / 2)
        message = ('Solo mode activated! Boss stats increased (Hp: ' + str(self.Hp) + ', Damage: ' + str(self.Dmg) + ', Defense: ' + str(self.Def) + ')')
        if MySet.Mode == 0:
            Parent.SendStreamMessage(message)
        elif MySet.Mode == 1:
            Parent.SendDiscordMessage(message)

    def scale_hp(self, num_players):
        self.Hp = self.Hp * num_players

class BossFight:
    def __init__(self):
        self.Boss = None
        self.IsActive = False
        self.Challengers = PlayerList()
        self.TimeToStart = 0

    def call_boss(self, boss):
        self.Boss = boss
        self.IsActive = True
        self.TimeToStart = MySet.BossDelay

    def enter_player(self, player, aggro):
        self.Challengers.add(player, aggro)

    def is_playing(self, player_id):
        player = self.Challengers.search_by_id(player_id)
        if player != None:
            return True
        return False

    def save_players(self, player_db):
        self.Challengers.save_listed_players(player_db)

    def battle(self, player_db):
        SoloMode = False
        if self.Boss == None:
            message = 'There is no boss here'
            if MySet.Mode == 0:
                Parent.SendStreamMessage(message)
            elif MySet.Mode == 1:
                Parent.SendDiscordMessage(message)
        elif self.Challengers.lenght() > 0:
            if self.Challengers.lenght() == 1:
                SoloMode = True
                self.Boss.soloMode_scaling(3)
            self.Boss.scale_hp(self.Challengers.lenght())
            if self.Challengers.lenght() >= 20:
                self.Challengers.give_title('Now it\'s a party!')
            while (self.Challengers.lenght() > 0) and (self.Boss.Hp > 0):
                self.Boss.Hp -= self.Challengers.dmg_boss(self.Boss.Def)
                self.Challengers.round_fatigue()
                self.Challengers.take_dmg(player_db, self.Boss.Dmg)
            if self.Boss.Hp <= 0:
                if self.Challengers.lenght() > 0:
                    ExpPerPlayer = self.Boss.Exp
                    if ((SoloMode == False) and (self.Challengers.lenght() == 1)):
                        self.Challengers.give_title('Last one standing')
                    if self.Boss.Title != (''):
                        self.Challengers.give_title(self.Boss.Title)
                    self.Challengers.gain_exp(self.Boss.Exp)
                    currency_gain = self.Boss.Exp * 40
                    self.Challengers.gain_currency(currency_gain)
                    message = ('The ' + self.Boss.Name + ' was defeated! Everyone gained ' + str(self.Boss.Exp) + ' experience and ' + str(currency_gain) + " gold")
                    if MySet.Mode == 0:
                        Parent.SendStreamMessage(message)
                    elif MySet.Mode == 1:
                        Parent.SendDiscordMessage(message)
                    self.Challengers.cleanup()
                    self.Challengers.loot_chance(self.Boss.Level)
                else:
                    message = ('The ' + self.Boss.Name + ' was defeated... but sadly nobody survived the battle')
                    if MySet.Mode == 0:
                        Parent.SendStreamMessage(message)
                    elif MySet.Mode == 1:
                        Parent.SendDiscordMessage(message)
            else:
                message = ('The ' + self.Boss.Name + ' stands victorious before the defeated challengers with ' + str(self.Boss.Hp) + ' hp.')
                if MySet.Mode == 0:
                    Parent.SendStreamMessage(message)
                elif MySet.Mode == 1:
                    Parent.SendDiscordMessage(message)
            self.Challengers.battle_summary()

#---------------------------------------
# Player Database functions
#---------------------------------------
def get_player_stats(player):
    if player != None:
        titles = player.get_titles()
        player_stats = "Name: " + player.Name + ", Class: " + player.Class + ", Level: " + str(player.level_calc()) + ", Exp: " + str(player.Exp) + ", Hp: " + str(player.Hp) + "/" + str(player.get_max_hp()) + ", Stamina: " + str(player.Stm) + "/" + str(player.get_max_stm()) + ", Damage: " + str(player.get_dmg()) + ", Defense: " + str(player.get_def()) + ", Dexterity: " + str(player.get_dex()) + ", Evasion: " + str(player.get_eva()) + ", Boss kills: " + str(player.KillCount) + ", Prestige level: " + str(player.ResetCount) + ", Item: " + str(player.Consumable.Description) + ", Gold: " + str(player.Currency)
        return player_stats
    return ""

def get_player_titles(player):
    if player != None:
        titles = player.get_titles()
        if titles == '':
            titles = 'None'
        message = player.Name + "\'s titles: " + titles
        return message
    return ""

def search_player_by_id(c, player_id):
    c.execute("SELECT * FROM players WHERE userId=:userId", {'userId': player_id})
    playerArray = c.fetchone()
    if playerArray != None:
        Pclass = playerArray[0]
        Titles_string = playerArray[12]
        player_titles = TitleList()
        for x in Titles_string.split(', '):
            player_titles.add_title(x)
        if Pclass == "Adventurer":
            auxplayer = Player(playerArray[1], playerArray[2], playerArray[3], playerArray[4], playerArray[5], playerArray[6], playerArray[7], playerArray[8], playerArray[9], playerArray[10], playerArray[11], player_titles, playerArray[13])
        elif Pclass == "Warrior":
            auxplayer = Warrior(playerArray[1], playerArray[2], playerArray[3], playerArray[4], playerArray[5], playerArray[6], playerArray[7], playerArray[8], playerArray[9], playerArray[10], playerArray[11], player_titles, playerArray[13])
        elif Pclass == "Barbarian":
            auxplayer = Barbarian(playerArray[1], playerArray[2], playerArray[3], playerArray[4], playerArray[5], playerArray[6], playerArray[7], playerArray[8], playerArray[9], playerArray[10], playerArray[11], player_titles, playerArray[13])
        elif Pclass == "Knight":
            auxplayer = Knight(playerArray[1], playerArray[2], playerArray[3], playerArray[4], playerArray[5], playerArray[6], playerArray[7], playerArray[8], playerArray[9], playerArray[10], playerArray[11], player_titles, playerArray[13])
        elif Pclass == "Scout":
            auxplayer = Scout(playerArray[1], playerArray[2], playerArray[3], playerArray[4], playerArray[5], playerArray[6], playerArray[7], playerArray[8], playerArray[9], playerArray[10], playerArray[11], player_titles, playerArray[13])
        elif Pclass == "Assassin":
            auxplayer = Assassin(playerArray[1], playerArray[2], playerArray[3], playerArray[4], playerArray[5], playerArray[6], playerArray[7], playerArray[8], playerArray[9], playerArray[10], playerArray[11], player_titles, playerArray[13])
        elif Pclass == "Archer":
            auxplayer = Archer(playerArray[1], playerArray[2], playerArray[3], playerArray[4], playerArray[5], playerArray[6], playerArray[7], playerArray[8], playerArray[9], playerArray[10], playerArray[11], player_titles, playerArray[13])
        elif Pclass == "Mage":
            auxplayer = Mage(playerArray[1], playerArray[2], playerArray[3], playerArray[4], playerArray[5], playerArray[6], playerArray[7], playerArray[8], playerArray[9], playerArray[10], playerArray[11], player_titles, playerArray[13])
        elif Pclass == "Wizard":
            auxplayer = Wizard(playerArray[1], playerArray[2], playerArray[3], playerArray[4], playerArray[5], playerArray[6], playerArray[7], playerArray[8], playerArray[9], playerArray[10], playerArray[11], player_titles, playerArray[13])
        elif Pclass == "Illusionist":
            auxplayer = Illusionist(playerArray[1], playerArray[2], playerArray[3], playerArray[4], playerArray[5], playerArray[6], playerArray[7], playerArray[8], playerArray[9], playerArray[10], playerArray[11], player_titles, playerArray[13])
        elif Pclass == "Cleric":
            auxplayer = Cleric(playerArray[1], playerArray[2], playerArray[3], playerArray[4], playerArray[5], playerArray[6], playerArray[7], playerArray[8], playerArray[9], playerArray[10], playerArray[11], player_titles, playerArray[13])
        elif Pclass == "Priest":
            auxplayer = Priest(playerArray[1], playerArray[2], playerArray[3], playerArray[4], playerArray[5], playerArray[6], playerArray[7], playerArray[8], playerArray[9], playerArray[10], playerArray[11], player_titles, playerArray[13])
        elif Pclass == "Paladin":
            auxplayer = Paladin(playerArray[1], playerArray[2], playerArray[3], playerArray[4], playerArray[5], playerArray[6], playerArray[7], playerArray[8], playerArray[9], playerArray[10], playerArray[11], player_titles, playerArray[13])
        elif Pclass == "Shaman":
            auxplayer = Shaman(playerArray[1], playerArray[2], playerArray[3], playerArray[4], playerArray[5], playerArray[6], playerArray[7], playerArray[8], playerArray[9], playerArray[10], playerArray[11], player_titles, playerArray[13])
        elif Pclass == "Warlock":
            auxplayer = Warlock(playerArray[1], playerArray[2], playerArray[3], playerArray[4], playerArray[5], playerArray[6], playerArray[7], playerArray[8], playerArray[9], playerArray[10], playerArray[11], player_titles, playerArray[13])
        elif Pclass == "Druid":
            auxplayer = Druid(playerArray[1], playerArray[2], playerArray[3], playerArray[4], playerArray[5], playerArray[6], playerArray[7], playerArray[8], playerArray[9], playerArray[10], playerArray[11], player_titles, playerArray[13])
        elif Pclass == "Naturalist":
            auxplayer = Naturalist(playerArray[1], playerArray[2], playerArray[3], playerArray[4], playerArray[5], playerArray[6], playerArray[7], playerArray[8], playerArray[9], playerArray[10], playerArray[11], player_titles, playerArray[13])
        elif Pclass == "Shapeshifter":
            auxplayer = Shapeshifter(playerArray[1], playerArray[2], playerArray[3], playerArray[4], playerArray[5], playerArray[6], playerArray[7], playerArray[8], playerArray[9], playerArray[10], playerArray[11], player_titles, playerArray[13])
        elif Pclass == "Elementalist":
            auxplayer = Elementalist(playerArray[1], playerArray[2], playerArray[3], playerArray[4], playerArray[5], playerArray[6], playerArray[7], playerArray[8], playerArray[9], playerArray[10], playerArray[11], player_titles, playerArray[13])
        elif Pclass == "Hydromancer":
            auxplayer = Hydromancer(playerArray[1], playerArray[2], playerArray[3], playerArray[4], playerArray[5], playerArray[6], playerArray[7], playerArray[8], playerArray[9], playerArray[10], playerArray[11], player_titles, playerArray[13])
        elif Pclass == "Aeromancer":
            auxplayer = Aeromancer(playerArray[1], playerArray[2], playerArray[3], playerArray[4], playerArray[5], playerArray[6], playerArray[7], playerArray[8], playerArray[9], playerArray[10], playerArray[11], player_titles, playerArray[13])
        elif Pclass == "Pyromancer":
            auxplayer = Pyromancer(playerArray[1], playerArray[2], playerArray[3], playerArray[4], playerArray[5], playerArray[6], playerArray[7], playerArray[8], playerArray[9], playerArray[10], playerArray[11], player_titles, playerArray[13])
        elif Pclass == "Geomancer":
            auxplayer = Geomancer(playerArray[1], playerArray[2], playerArray[3], playerArray[4], playerArray[5], playerArray[6], playerArray[7], playerArray[8], playerArray[9], playerArray[10], playerArray[11], player_titles, playerArray[13])
        return auxplayer
    else:
        return None

def player_ko(c, player_id):
    player = search_player_by_id(c, player_id)
    if player != None:
        LossMessage = player.death_penalty()
        save_player(c, player)
        message = (player.Name + " was knocked out " + LossMessage)
        if MySet.Mode == 0:
            Parent.SendStreamMessage(message)
        elif MySet.Mode == 1:
            Parent.SendDiscordMessage(message)

def delete_player(c, player_id):
    c.execute("SELECT * FROM players WHERE userId=:userId", {'userId': player_id})
    playerArray = c.fetchone()
    if playerArray != None:
        message = ("The one known as " + playerArray[1] + " has perished on this tragic day, may their soul rest in peace")
        if MySet.Mode == 0:
            Parent.SendStreamMessage(message)
        elif MySet.Mode == 1:
            Parent.SendDiscordMessage(message)
        c.execute("DELETE from players WHERE userId=:userId", {'userId': player_id})

def save_player(c, player):
    c.execute("SELECT * FROM players WHERE userId=:userId", {'userId': player.UserId})
    playerArray = c.fetchone()
    Titles = player.get_titles()
    if playerArray == None:
        c.execute("INSERT INTO players VALUES (:class, :userId, :name, :exp, :hp, :stm, :dmg, :def, :dex, :eva, :kc, :rc, :titles, :gold)", {'class': player.Class, 'userId': player.UserId, 'name': player.Name, 'exp': player.Exp, 'hp': player.Hp, 'stm': player.Stm, 'dmg': player.Dmg, 'def': player.Def, 'dex': player.Dex, 'eva': player.Eva, 'kc': player.KillCount, 'rc': player.ResetCount, 'titles': Titles, 'gold': player.Currency})
    else:   
        c.execute("UPDATE players SET class = :class WHERE userId=:userId", {'userId': player.UserId, 'class': player.Class})
        c.execute("UPDATE players SET exp = :exp WHERE userId=:userId", {'userId': player.UserId, 'exp': player.Exp})
        c.execute("UPDATE players SET hp = :hp WHERE userId=:userId", {'userId': player.UserId, 'hp': player.Hp})
        c.execute("UPDATE players SET stm = :stm WHERE userId=:userId", {'userId': player.UserId, 'stm': player.Stm})
        c.execute("UPDATE players SET dmg = :dmg WHERE userId=:userId", {'userId': player.UserId, 'dmg': player.Dmg})
        c.execute("UPDATE players SET def = :def  WHERE userId=:userId", {'userId': player.UserId, 'def': player.Def})
        c.execute("UPDATE players SET dex = :dex WHERE userId=:userId", {'userId': player.UserId, 'dex': player.Dex})
        c.execute("UPDATE players SET eva = :eva WHERE userId=:userId", {'userId': player.UserId, 'eva': player.Eva})
        c.execute("UPDATE players SET kc = :kc WHERE userId=:userId", {'userId': player.UserId, 'kc': player.KillCount})
        c.execute("UPDATE players SET rc = :rc WHERE userId=:userId", {'userId': player.UserId, 'rc': player.ResetCount})
        c.execute("UPDATE players SET titles = :titles WHERE userId=:userId", {'userId': player.UserId, 'titles': Titles})
        c.execute("UPDATE players SET gold = :gold WHERE userId=:userId", {'userId': player.UserId, 'gold': player.Currency})

def create_db(c):
    c.execute("""CREATE TABLE IF NOT EXISTS players (
            class text,
            userId text,
            name text,
            exp int,
            hp int,
            stm int,
            dmg int,
            def int,
            dex int,
            eva int,
            kc int,
            rc int,
            titles text,
            gold int
            )""")

def search_boss_by_lvl(b, lvl):
    b.execute("SELECT * FROM bosses WHERE level=:level", {'level': lvl})
    bossesArray = b.fetchall()
    if bossesArray != []:
        index = Parent.GetRandom(0,len(bossesArray))
        bossArray = bossesArray[index]
        auxboss = Boss(bossArray[0], bossArray[1], bossArray[2], bossArray[3], bossArray[4], bossArray[5], bossArray[6], bossArray[7])
        return auxboss
    else:
        return None

def player_can_reset(player):
    if player.level_calc() == 20 :
        return True
    return False

#---------------------------------------
# [OPTIONAL] Settings functions
#---------------------------------------
def SetDefaults():
    """Set default settings function"""

    #play windows sound
    winsound.MessageBeep()

    #open messagebox with a security check
    MessageBox = ctypes.windll.user32.MessageBoxW
    returnValue = MessageBox(0, u"You are about to reset the settings, "
                                "are you sure you want to contine?"
                             , u"Reset settings file?", 4)

    #if user press "yes"
    if returnValue == 6:

        # Save defaults back to file
        Settings.SaveSettings(MySet, settingsFile)

        #show messagebox that it was complete
        MessageBox = ctypes.windll.user32.MessageBoxW
        returnValue = MessageBox(0, u"Settings successfully restored to default values"
                                 , u"Reset complete!", 0)

#---------------------------------------
# [Required] shop item class
#---------------------------------------
class Item(object):
    def __init__(self, desc, cost):
        self.Description = desc
        self.Cost = cost

#---------------------------------------
# [Required] Consumable classes
#---------------------------------------

# stash class (Consumable map)
class Stash(object):
    def __init__(self, stash_array):
        self.ConsumablesList = {} #dictionary will hold a duplet of (consumable_code, quantity)
        self.ConsumablesList.append((0, -1))
        if (stash_array != ''):
            splitted_stash = stash_array.split(',')
            Len = len(splitted_stash)
            i = 0
            while (i < Len):
                splitted_item = splitted_stash[i].split('-')
                self.ConsumablesList.append(int(splitted_item[0]), int(splitted_item[1]))

    def into_array(self):
        Arr = ""
        Len = len(self.ConsumablesList)
        i = 1
        while (i < Len):
            (code, quan) = self.ConsumablesList[i]
            Arr += str(code) + "-" + str(quan)
            i += 1
            if i < Len:
                Arr += ","

    def add_item(self, index):
        Len = len(self.ConsumablesList)
        i = 1
        while (i < Len):
            (code, quan) = self.ConsumablesList[i]
            if (code == index) and (AllConsumables[code].Stack_size > quan):
                self.ConsumablesList[i] = (code, quan + 1)
        if (i == Len):
            if (self.ConsumablesList[i] == None) and (i <= 12):
                self.ConsumablesList.append(index, 1)
            else:
                self.ConsumablesList[i] = (index, 1)

    def remove_item(self, index):
        Len = len(self.ConsumablesList)
        i = 1
        while (i < Len):
            (code, quan) = self.ConsumablesList[i]
            if (code == index):
                if quan > 1:
                    self.ConsumablesList[i] = (code, quan - 1)
                else:
                    self.ConsumablesList.pop(i)

# generic object class
class Consumable(object):
    def __init__(self, index, name, rounds, power, stack_size):
        self.Index = index # code of the item / index in the master consumable list (AllConsumables)
        self.Name = name # name of the item
        self.Rounds = rounds # rounds of use, this should be checked using != 0 because that way we can use -1 for items that last the whole combat
        self.Power = power # general power of the item, to be used in it's own funtionality
        self.Stack_size = stack_size

    # receives the owner player to apply buffs/heals
    def standby(self, player):
        return

    # returns pure damage to deal
    def standby_pure(self):
        return 0

    # receives boss' defense and returns damage done
    def standby_attack(self, boss_def):
        return 0

    # receives the owner player and damage done for buff/heals dependent on the damage done
    def after_attack(self, player, player_dmg):
        return

# Basic stat boost item
class Dmg_boost_item(Consumable):
    def standby(self, player):
        if self.Rounds != 0:
            player.BonusDmg = self.Power
            self.Rounds -= 1

# Basic stat boost item
class Def_boost_item(Consumable):
    def standby(self, player):
        if self.Rounds != 0:
            player.BonusDef = self.Power
            self.Rounds -= 1

# Basic stat boost item
class Dex_boost_item(Consumable):
    def standby(self, player):
        if self.Rounds != 0:
            player.BonusDex = self.Power
            self.Rounds -= 1

# Basic stat boost item
class Eva_boost_item(Consumable):
    def standby(self, player):
        if self.Rounds != 0:
            player.BonusEva = self.Power
            self.Rounds -= 1

# Item that heals
class Heal_item(Consumable):
    def standby(self, player):
        if self.Rounds != 0:
            player.add_hp(self.Power)
            self.Rounds -= 1

# Item that does pure damage
class Pure_damage_item(Consumable):
    def standby_pure(self):
        if self.Rounds != 0:
            self.Rounds -= 1
            return self.Power

# Item that does damage against boss defense
class Attack_item(Consumable):
    def standby_pure(self, boss_def):
        if self.Rounds != 0:
            self.Rounds -= 1
            attack = self.Power
            MaxDmgBlock = int(attack * 0.75)
            if boss_def > MaxDmgBlock:
                dmg = attack - MaxDmgBlock
            else:
                dmg = attack - boss_def
            return dmg

# Item with effects based on damage done
class After_attack_item(consumable):
    def after_attack(self, player, player_dmg):
        multiplier = 0.25
        heal = int(multiplier * self.Power * player_dmg)
        player.add_hp(heal)

#---------------------------------------
# [Required] functions
#---------------------------------------
def Init():
    """data on Load, required function"""
    global MySet
    global MyBoss
    global LootMesDmg
    global LootMesDef
    global LootMesDex
    global LootMesEva
    global ShopItems
    global AllConsumables

    MySet = Settings(settingsFile)
    MyBoss = BossFight()

    LootMesDmg = ['a magic ring','a magic potion','a strong bracelet','an enchanted necklace']
    LootMesDef = ['a magic ring','a magic potion','a sturdy bracelet','an enchanted necklace']
    LootMesDex = ['a magic ring','a magic potion','a keen bracelet','an enchanted necklace']
    LootMesEva = ['a magic ring','a magic potion','a light bracelet','an enchanted necklace']

    ShopItems[0] = Item('25 hp heal', 150)
    ShopItems[1] = Item('25 stm recovery', 100)
    ShopItems[2] = Item('10 exp', 500)

    # Consumable __init__(self, index, name, rounds, power, stack_size)
    AllConsumables[0] = Consumable(0, 'Unequip', 0, 0, -1)
    AllConsumables[1] = Dmg_boost_item(1, 'Red boost orb', 1, 5, 5)
    AllConsumables[2] = Def_boost_item(2, 'Blue boost orb', 1, 10, 5)
    AllConsumables[3] = Dex_boost_item(3, 'Yellow boost orb', 1, 15, 5)
    AllConsumables[4] = Eva_boost_item(4, 'Green boost orb', 1, 15, 5)
    AllConsumables[5] = Heal_item(5, 'Healing salve', 4, 10, 5)
    AllConsumables[6] = Heal_item(6, 'Ancient Tango', 10, 3, 5)
    AllConsumables[7] = Pure_damage_item(7, 'Flame medallion', 5, 5, 2)
    AllConsumables[8] = Pure_damage_item(8, 'Snowball', 1, 1, 10)
    AllConsumables[9] = Attack_item(9, 'Frost spear', 1, 40, 1)
    AllConsumables[10] = Heal_item(10, 'Harpy tear', 2, 200, 1)
    AllConsumables[11] = After_attack_item(11, 'Vampire fang', 5, 1, 1)

def Execute(data):
    """Required Execute data function"""
    if data.IsWhisper() and (data.GetParam(0).lower() == MySet.ShopCommand.lower() or data.GetParam(0).lower() == MySet.TitlesCommand.lower() or data.GetParam(0).lower() == MySet.WorkCommand.lower() or data.GetParam(0).lower() == MySet.ClassChange.lower() or data.GetParam(0).lower() == MySet.AllBossCD.lower() or data.GetParam(0).lower() == MySet.PrestigeCommand.lower() or data.GetParam(0).lower() == MySet.NewCharacterCommand.lower() or data.GetParam(0).lower() == MySet.StatsCommand.lower() or data.GetParam(0).lower() == MySet.DungeonCommand.lower() or data.GetParam(0).lower() == MySet.BossCommand.lower() or data.GetParam(0).lower() == MySet.CallBossCommand.lower() or data.GetParam(0).lower() == MySet.RestCommand.lower() or data.GetParam(0).lower() == MySet.InfoCommand.lower()):
        if not Parent.HasPermission(data.User, MySet.Permission, MySet.PermissionResp):
            message = MySet.PermissionResp.format(data.User, MySet.Permission, MySet.PermissionResp)
            if MySet.Mode == 0:
                Parent.SendStreamWhisper(data.UserName, message)
            elif MySet.Mode == 1:
                Parent.SendDiscordDM(data.UserName, message)
            return

        if not HasPermission(data):
            return

        if not data.IsFromTwitch() and MySet.Mode == 0:
            return

        if not data.IsFromDiscord() and MySet.Mode == 1:
            return

        if MyBoss.is_playing(data.User):
            message = ('You are busy preparing for the boss fight.')
            if MySet.Mode == 0:
                Parent.SendStreamWhisper(data.UserName, message)
            elif MySet.Mode == 1:
                Parent.SendDiscordDM(data.UserName, message)
            return

        database = os.path.join(os.path.dirname(__file__), 'RpgDB.db')
        conn = sqlite3.connect(database)
        c = conn.cursor()
        create_db(c)

#---------------------------------------class change---------------------------------------
        if data.GetParam(0).lower() == MySet.ClassChange.lower():
            player = search_player_by_id(c, data.User)
            nClass = data.GetParam(1).lower()
            nPlayer = class_change(player, nClass)
            if nPlayer != None:
                free_rest(nPlayer)
                save_player(c, nPlayer)

#---------------------------------------all bosses cooldowns---------------------------------------
        if data.GetParam(0).lower() == MySet.AllBossCD.lower():
            message = allBossesCooldowns()
            if MySet.Mode == 0:
                Parent.SendStreamWhisper(data.UserName, message)
            elif MySet.Mode == 1:
                Parent.SendDiscordDM(data.UserName, message)

#---------------------------------------prestige---------------------------------------
        if data.GetParam(0).lower() == MySet.PrestigeCommand.lower():
            player = search_player_by_id(c, data.User)
            if player != None:
                if player_can_reset(player):
                    newPlayer = Player(data.User, data.UserName, 0, 50, 100, 1, 1, 1, 1, player.KillCount, (int(player.ResetCount) + 1), player.Titles, player.Currency, player.Consumable, player.Stash)
                    if newPlayer.ResetCount >= 5:
                        newPlayer.gain_title('Here we go again')
                    save_player(c, newPlayer)
                    message = ('The character ' + data.UserName + ' has been reset and gained a prestige level.')
                    if MySet.Mode == 0:
                        Parent.SendStreamMessage(message)
                    elif MySet.Mode == 1:
                        Parent.SendDiscordMessage(message)
                else:
                    message = ('You are not ready for a reset yet.')
                    if MySet.Mode == 0:
                        Parent.SendStreamWhisper(data.UserName, message)
                    elif MySet.Mode == 1:
                        Parent.SendDiscordDM(data.UserName, message)
            else:
                message = ('You don\'t have a character, type !rpginfo to know more.')
                if MySet.Mode == 0:
                    Parent.SendStreamWhisper(data.UserName, message)
                elif MySet.Mode == 1:
                    Parent.SendDiscordDM(data.UserName, message)

#---------------------------------------info---------------------------------------
        if data.GetParam(0).lower() == MySet.InfoCommand.lower():
            message = MySet.Info.format(MySet.NewCharacterCommand, MySet.StatsCommand, MySet.DungeonCommand, MySet.CallBossCommand, MySet.RestCommand)
            if MySet.Mode == 0:
                Parent.SendStreamWhisper(data.UserName, message)
            elif MySet.Mode == 1:
                Parent.SendDiscordDM(data.UserName, message)

#---------------------------------------new character---------------------------------------
        if data.GetParam(0).lower() == MySet.NewCharacterCommand.lower():
            userId = search_player_by_id(c, data.User)
            if userId == None:
                titles = TitleList()
                stash = ItemsList()
                newPlayer = Player(data.User, data.UserName, 0, 50, 100, 1, 1, 1, 1, 0, 0, titles, 500, None, stash)
                save_player(c, newPlayer)
                message = ('The character ' + data.UserName + ' has been successfully created.')
                if MySet.Mode == 0:
                    Parent.SendStreamWhisper(data.UserName, message)
                elif MySet.Mode == 1:
                    Parent.SendDiscordDM(data.UserName, message)
            else:
                message = ('You already have a character.')
                if MySet.Mode == 0:
                    Parent.SendStreamWhisper(data.UserName, message)
                elif MySet.Mode == 1:
                    Parent.SendDiscordDM(data.UserName, message)

#---------------------------------------my stats---------------------------------------                
        if data.GetParam(0).lower() == MySet.StatsCommand.lower():
            player = search_player_by_id(c, data.User)
            if player != None:
                message = get_player_stats(player)
                if MySet.Mode == 0:
                    Parent.SendStreamWhisper(data.UserName, message)
                elif MySet.Mode == 1:
                    Parent.SendDiscordDM(data.UserName, message)
            else:
                message = ('You don\'t have a character, type !rpginfo to know more.')
                if MySet.Mode == 0:
                    Parent.SendStreamWhisper(data.UserName, message)
                elif MySet.Mode == 1:
                    Parent.SendDiscordDM(data.UserName, message)

#---------------------------------------my titles---------------------------------------                
        if data.GetParam(0).lower() == MySet.TitlesCommand.lower():
            player = search_player_by_id(c, data.User)
            if player != None:
                message = get_player_titles(player)
                if MySet.Mode == 0:
                    Parent.SendStreamWhisper(data.UserName, message)
                elif MySet.Mode == 1:
                    Parent.SendDiscordDM(data.UserName, message)
            else:
                message = ('You don\'t have a character, type !rpginfo to know more.')
                if MySet.Mode == 0:
                    Parent.SendStreamWhisper(data.UserName, message)
                elif MySet.Mode == 1:
                    Parent.SendDiscordDM(data.UserName, message)

#---------------------------------------rest---------------------------------------                
        if (data.GetParam(0).lower() == MySet.RestCommand.lower()) and (restIsOnCooldown(data) is False):
            player = search_player_by_id(c, data.User)
            if player != None:
                rest(data, player)
                save_player(c, player)
                message = ('You rest and recover ' +  str(MySet.RestHP) + '% health and ' + str(MySet.RestStm) + '% stamina.')
                if MySet.Mode == 0:
                    Parent.SendStreamWhisper(data.UserName, message)
                elif MySet.Mode == 1:
                    Parent.SendDiscordDM(data.UserName, message)
            else:
                message = ('You don\'t have a character, type !rpginfo to know more.')
                if MySet.Mode == 0:
                    Parent.SendStreamWhisper(data.UserName, message)
                elif MySet.Mode == 1:
                    Parent.SendDiscordDM(data.UserName, message)

#---------------------------------------work---------------------------------------
        if (data.GetParam(0).lower() == MySet.WorkCommand.lower()):
            player = search_player_by_id(c, data.User)
            if player == None:
                message = ('You don\'t have a character, type !rpginfo to know more.')
                if MySet.Mode == 0:
                    Parent.SendStreamWhisper(data.UserName, message)
                elif MySet.Mode == 1:
                    Parent.SendDiscordDM(data.UserName, message)
            else:
                if player.Stm < 25:
                    message = ('You are too tired to work, rest until you have at least 25 energy.')
                    if MySet.Mode == 0:
                        Parent.SendStreamWhisper(data.UserName, message)
                    elif MySet.Mode == 1:
                        Parent.SendDiscordDM(data.UserName, message)
                else:
                    player.add_stm(-25)
                    roll = Parent.GetRandom(1,101)
                    loot = 0
                    if roll < 25:
                        loot = 20
                    elif roll < 50:
                        loot = 40
                    else:
                        loot = roll
                    player.add_currency(loot)    
                    message = ('You work and gain ' + str(loot) + ' gold')
                    save_player(c, player)
                    if MySet.Mode == 0:
                        Parent.SendStreamWhisper(data.UserName, message)
                    elif MySet.Mode == 1:
                        Parent.SendDiscordDM(data.UserName, message)

#---------------------------------------dungeon crawl---------------------------------------
        if (data.GetParam(0).lower() == MySet.DungeonCommand.lower()) and (dungeonIsOnCooldown(data) is False):
            player = search_player_by_id(c, data.User)
            if player == None:
                message = ('You don\'t have a character, type !rpginfo to know more.')
                if MySet.Mode == 0:
                    Parent.SendStreamWhisper(data.UserName, message)
                elif MySet.Mode == 1:
                    Parent.SendDiscordDM(data.UserName, message)
            else:
                if player.get_currency() < MySet.Cost:
                    message = MySet.ResponseNotEnoughPoints.format(data.UserName, MySet.Cost, "gold")
                    if MySet.Mode == 0:
                        Parent.SendStreamWhisper(data.UserName, message)
                    elif MySet.Mode == 1:
                        Parent.SendDiscordDM(data.UserName, message)
                else:
                    if player.Stm < 25:
                        message = ('You are too tired to go into the dungeon, rest until you have at least 25 energy.')
                        if MySet.Mode == 0:
                            Parent.SendStreamWhisper(data.UserName, message)
                        elif MySet.Mode == 1:
                            Parent.SendDiscordDM(data.UserName, message)
                    else:
                        player.add_currency(-MySet.Cost)
                        alive = 1
                        start = int(1 + int(player.DungeonBonus))
                        roll = Parent.GetRandom(start,100)
                        if roll <= 25:
                            startingHp = player.Hp
                            dungeon_dmg(player, roll-31)
                            newHp = player.Hp
                            save_player(c, player)
                            if newHp == 0:
                                alive = 0
                                player_ko(c, data.User)
                            else:
                                message = ('You wake up outside of the dungeon hurting all over, you gained no experience and took ' + str(startingHp - newHp) + ' damage.')
                                if MySet.Mode == 0:
                                    Parent.SendStreamWhisper(data.UserName, message)
                                elif MySet.Mode == 1:
                                    Parent.SendDiscordDM(data.UserName, message)                      
                        elif roll <= 50:
                            message = ('You barely escaped the dungeon before taking any damage, but gained no experience.')
                            if MySet.Mode == 0:
                                Parent.SendStreamWhisper(data.UserName, message)
                            elif MySet.Mode == 1:
                                Parent.SendDiscordDM(data.UserName, message)
                            lootmax = int(16 - int(player.LootBonus)) 
                            lootRoll = Parent.GetRandom(1, lootmax + 1)
                            if lootRoll <= 4:
                                stats_gained = int(player.level_calc() / 3)
                                if stats_gained == 0:
                                    stats_gained = 1
                                if lootRoll == 1:
                                    sGained = player.add_dmg(stats_gained)
                                    MaxIn = len(LootMesDmg)
                                    lootIndex = Parent.GetRandom(0, MaxIn)
                                    loot = LootMesDmg[lootIndex]
                                    messageLoot = ('You found ' + loot + ', +' + str(sGained) + ' Damage.')
                                elif lootRoll == 2:
                                    sGained = player.add_def(stats_gained)
                                    MaxIn = len(LootMesDef)
                                    lootIndex = Parent.GetRandom(0, MaxIn)
                                    loot = LootMesDef[lootIndex]
                                    messageLoot = ('You found ' + loot + ', +' + str(sGained) + ' Defense.')
                                elif lootRoll == 3:
                                    sGained = player.add_dex(stats_gained)
                                    MaxIn = len(LootMesDex)
                                    lootIndex = Parent.GetRandom(0, MaxIn)
                                    loot = LootMesDex[lootIndex]
                                    messageLoot = ('You found ' + loot + ', +' + str(sGained) + ' Dexterity.')
                                elif lootRoll == 4:
                                    sGained = player.add_eva(stats_gained)
                                    MaxIn = len(LootMesEva)
                                    lootIndex = Parent.GetRandom(0, MaxIn)
                                    loot = LootMesEva[lootIndex]
                                    messageLoot = ('You found ' + loot + ', +' + str(sGained) + ' Evasion.')
                                if MySet.Mode == 0:
                                    Parent.SendStreamWhisper(data.UserName, messageLoot)
                                elif MySet.Mode == 1:
                                    Parent.SendDiscordDM(data.UserName, messageLoot)
                        elif roll <= 75:
                            expGain = Parent.GetRandom(2,7)
                            exp_gained = int(expGain/2)
                            eGained = player.add_exp(exp_gained)
                            startingHp = player.Hp
                            dungeon_dmg(player, roll-76)
                            newHp = player.Hp
                            if newHp == 0:
                                alive = 0
                                player_ko(c, data.User)
                            else:
                                message = ('You leave the dungeon with some scratches after a fierce adventure, you gained ' + str(eGained) + ' experience and took ' + str(startingHp - newHp) + ' damage.')
                                if MySet.Mode == 0:
                                    Parent.SendStreamWhisper(data.UserName, message)
                                elif MySet.Mode == 1:
                                    Parent.SendDiscordDM(data.UserName, message)
                                lootmax = int(12 - int(player.LootBonus))
                                lootRoll = Parent.GetRandom(1,lootmax + 1)
                                if lootRoll <= 4:
                                    stats_gained = int(player.level_calc() / 3)
                                    if stats_gained == 0:
                                        stats_gained = 1
                                    if lootRoll == 1:
                                        sGained = player.add_dmg(stats_gained)
                                        MaxIn = len(LootMesDmg)
                                        lootIndex = Parent.GetRandom(0, MaxIn)
                                        loot = LootMesDmg[lootIndex]
                                        messageLoot = ('You found ' + loot + ', +' + str(sGained) + ' Damage.')
                                    elif lootRoll == 2:
                                        sGained = player.add_def(stats_gained)
                                        MaxIn = len(LootMesDef)
                                        lootIndex = Parent.GetRandom(0, MaxIn)
                                        loot = LootMesDef[lootIndex]
                                        messageLoot = ('You found ' + loot + ', +' + str(sGained) + ' Defense.')
                                    elif lootRoll == 3:
                                        sGained = player.add_dex(stats_gained)
                                        MaxIn = len(LootMesDex)
                                        lootIndex = Parent.GetRandom(0, MaxIn)
                                        loot = LootMesDex[lootIndex]
                                        messageLoot = ('You found ' + loot + ', +' + str(sGained) + ' Dexterity.')
                                    elif lootRoll == 4:
                                        sGained = player.add_eva(stats_gained)
                                        MaxIn = len(LootMesEva)
                                        lootIndex = Parent.GetRandom(0, MaxIn)
                                        loot = LootMesEva[lootIndex]
                                        messageLoot = ('You found ' + loot + ', +' + str(sGained) + ' Evasion.')
                                    if MySet.Mode == 0:
                                        Parent.SendStreamWhisper(data.UserName, messageLoot)
                                    elif MySet.Mode == 1:
                                        Parent.SendDiscordDM(data.UserName, messageLoot)
                        else:
                            expGain = Parent.GetRandom(1,4)
                            exp_gained = int(expGain)
                            eGained = player.add_exp(exp_gained)
                            message = ('You leave the dungeon untouched and filled with pride, you gained ' + str(eGained) + ' experience.')
                            if MySet.Mode == 0:
                                Parent.SendStreamWhisper(data.UserName, message)
                            elif MySet.Mode == 1:
                                Parent.SendDiscordDM(data.UserName, message)
                            lootmax = int(8 - int(player.LootBonus))
                            lootRoll = Parent.GetRandom(1,lootmax + 1)
                            if lootRoll <= 4:
                                stats_gained = int(player.level_calc() / 3)
                                if stats_gained == 0:
                                    stats_gained = 1
                                if lootRoll == 1:
                                    sGained = player.add_dmg(stats_gained)
                                    MaxIn = len(LootMesDmg)
                                    lootIndex = Parent.GetRandom(0, MaxIn)
                                    loot = LootMesDmg[lootIndex]
                                    messageLoot = ('You found ' + loot + ', +' + str(sGained) + ' Damage.')
                                elif lootRoll == 2:
                                    sGained = player.add_def(stats_gained)
                                    MaxIn = len(LootMesDef)
                                    lootIndex = Parent.GetRandom(0, MaxIn)
                                    loot = LootMesDef[lootIndex]
                                    messageLoot = ('You found ' + loot + ', +' + str(sGained) + ' Defense.')
                                elif lootRoll == 3:
                                    sGained = player.add_dex(stats_gained)
                                    MaxIn = len(LootMesDex)
                                    lootIndex = Parent.GetRandom(0, MaxIn)
                                    loot = LootMesDex[lootIndex]
                                    messageLoot = ('You found ' + loot + ', +' + str(sGained) + ' Dexterity.')
                                elif lootRoll == 4:
                                    sGained = player.add_eva(stats_gained)
                                    MaxIn = len(LootMesEva)
                                    lootIndex = Parent.GetRandom(0, MaxIn)
                                    loot = LootMesEva[lootIndex]
                                    messageLoot = ('You found ' + loot + ', +' + str(sGained) + ' Evasion.')
                                if MySet.Mode == 0:
                                    Parent.SendStreamWhisper(data.UserName, messageLoot)
                                elif MySet.Mode == 1:
                                    Parent.SendDiscordDM(data.UserName, messageLoot)
                        if alive == 1:
                            player.add_stm(-25)
                            save_player(c, player)
                        AddDungeonCooldown(data)

#---------------------------------------boss fight---------------------------------------
        if (data.GetParam(0).lower() == MySet.BossCommand.lower()):
            player = search_player_by_id(c, data.User)
            if not player == None:
                if MyBoss.IsActive:
                    p_lvl = player.level_calc() + 1
                    if (p_lvl < MyBoss.Boss.Level):
                        message = ('You are not ready for a fight of this level')
                        if MySet.Mode == 0:
                            Parent.SendStreamWhisper(data.UserName, message)
                        elif MySet.Mode == 1:
                            Parent.SendDiscordDM(data.UserName, message)
                    else:
                        aggro = data.GetParam(1).lower()
                        if not is_number(aggro):
                            aggro = 1
                        if (int(aggro) < 1):
                            aggro = 1
                        if (5 < int(aggro)):
                            aggro = 5
                        MyBoss.enter_player(player, int(aggro))
                        if MySet.Mode == 0:
                            message = (player.Name + ' was enlisted (' + str(aggro) + ')')
                            Parent.SendStreamMessage(message)
                        elif MySet.Mode == 1:
                            message = (player.Name + ' (level ' + str(player.level_calc()) + ' ' + player.Class + ') was enlisted (' + str(aggro) + ')')
                            Parent.SendDiscordMessage(message)
                else:
                    message = ('There are no bosses to fight right now, type ' + MySet.CallBossCommand + ' [level] to see if one answers the call')
                    if MySet.Mode == 0:
                        Parent.SendStreamWhisper(data.UserName, message)
                    elif MySet.Mode == 1:
                        Parent.SendDiscordDM(data.UserName, message)
            else:
                message = ('You don\'t have a character, type !rpginfo to know more.')
                if MySet.Mode == 0:
                    Parent.SendStreamWhisper(data.UserName, message)
                elif MySet.Mode == 1:
                    Parent.SendDiscordDM(data.UserName, message)

#---------------------------------------call boss fight---------------------------------------
        if (data.GetParam(0).lower() == MySet.CallBossCommand.lower()) and (MyBoss.IsActive is False):
            lvl = data.GetParam(1).lower()
            player = search_player_by_id(c, data.User)
            if not player == None:
                p_lvl = player.level_calc() + 1
                if not is_number(lvl):
                    lvl = 0
                if ((int(lvl) < 0) or (20 < int(lvl))):
                    lvl = 0
                if (p_lvl < int(lvl)):
                    message = ('You are not ready to summon a boss of that level')
                    if MySet.Mode == 0:
                        Parent.SendStreamWhisper(data.UserName, message)
                    elif MySet.Mode == 1:
                        Parent.SendDiscordDM(data.UserName, message)
                elif (callBossIsOnCooldown(int(lvl), data) is False):
                    bossDB = os.path.join(os.path.dirname(__file__), 'BossDB.db')
                    Bconn = sqlite3.connect(bossDB)
                    b = Bconn.cursor()
                    boss = search_boss_by_lvl(b, int(lvl))
                    if boss == None:
                        message = ('There are no bosses of that level, type ' + MySet.CallBossCommand + ' [level] to call a boss')
                        if MySet.Mode == 0:
                            Parent.SendStreamWhisper(data.UserName, message)
                        elif MySet.Mode == 1:
                            Parent.SendDiscordDM(data.UserName, message)
                    else:
                        MyBoss.Boss = boss
                        MyBoss.TimeToStart = time.time() + MySet.BossDelay
                        MyBoss.IsActive = True
                        message = ('The ' + boss.Name + ' (Level: ' + str(boss.Level) + ', Exp per player: ' + str(boss.Exp) + ', Hp per player: ' + str(boss.Hp) + ', Damage: ' + str(boss.Dmg) + ', Defense: ' + str(boss.Def) + ')' + ' has answered the call, type ' + MySet.BossCommand + ' to enlist for the fight')
                        if MySet.Mode == 0:
                            Parent.SendStreamMessage(message)
                        elif MySet.Mode == 1:
                            #if MyBoss.Boss.Png != "":
                                #message += ("\n" + MyBoss.Boss.Png)
                            Parent.SendDiscordMessage(message)
                    Bconn.commit()
                    Bconn.close()
            else:
                message = ('You don\'t have a character, type !rpginfo to know more.')
                if MySet.Mode == 0:
                    Parent.SendStreamWhisper(data.UserName, message)
                elif MySet.Mode == 1:
                    Parent.SendDiscordDM(data.UserName, message)

#---------------------------------------shop---------------------------------------
        if (data.GetParam(0).lower() == MySet.ShopCommand.lower()):
            item = data.GetParam(1).lower()
            if not is_number(item):
                item = -1
            item_index = int(item)
            if item_index < len(ShopItems) and item_index >= 0:
                cost = ShopItems[item_index].Cost
                player = search_player_by_id(c, data.User)
                if player.get_currency() >= cost:
                    if item_index == 0:
                        player.add_hp(25)
                    elif item_index == 1:
                        player.add_hp(50)
                    elif item_index == 2:
                        player.add_stm(25)
                    elif item_index == 3:
                        player.add_stm(50)
                    elif item_index == 4:
                        player.add_exp(10)
                    player.add_currency(-cost)
                    save_player(c, player)
                    message = ('You bought ' + ShopItems[item_index].Description + ' for ' + str(ShopItems[item_index].Cost) + ' gold')
                    if MySet.Mode == 0:
                        Parent.SendStreamWhisper(data.UserName, message)
                    elif MySet.Mode == 1:
                        Parent.SendDiscordDM(data.UserName, message)
                else:
                    message = ('You don\'t have '+ str(ShopItems[item_index].Cost) +' gold to afford ' + ShopItems[item_index].Description)
                    if MySet.Mode == 0:
                        Parent.SendStreamWhisper(data.UserName, message)
                    elif MySet.Mode == 1:
                        Parent.SendDiscordDM(data.UserName, message)
            else:
                message = ('There is no item with that code')
                if MySet.Mode == 0:
                    Parent.SendStreamWhisper(data.UserName, message)
                elif MySet.Mode == 1:
                    Parent.SendDiscordDM(data.UserName, message)

#---------------------------------------close database---------------------------------------
        conn.commit()
        conn.close()

    return

def Tick():
#---------------------------------------Boss fight tick---------------------------------------
    if(MyBoss.IsActive == True and time.time() >= MyBoss.TimeToStart):
        if MyBoss.Challengers.lenght() > 0:
            boss_level = MyBoss.Boss.Level
            message = ('The battle with the ' + MyBoss.Boss.Name + ' has begun!')
            if MySet.Mode == 0:
                Parent.SendStreamMessage(message)
            elif MySet.Mode == 1:
                Parent.SendDiscordMessage(message)
            database = os.path.join(os.path.dirname(__file__), 'RpgDB.db')
            conn = sqlite3.connect(database)
            c = conn.cursor()
            create_db(c)
            MyBoss.battle(c)
            MyBoss.save_players(c)
            MyBoss = BossFight()
            conn.commit()
            conn.close()
            AddCallBossCooldown(boss_level)
        else:
            message = ('The ' + MyBoss.Boss.Name + ' has retired because no one appeared')
            if MySet.Mode == 0:
                Parent.SendStreamMessage(message)
            elif MySet.Mode == 1:
                Parent.SendDiscordMessage(message)
            MyBoss = BossFight()
    return

#-------------------------------------------------------------
# [Required] Percentual damage from dungeons function
#-------------------------------------------------------------

def dungeon_dmg(player, damage):
    dmgToTake = int(damage * player.get_max_hp() / 100) + 1
    player.add_hp(dmgToTake)

#-------------------------------------------------------------
# [Required] Rest function
#-------------------------------------------------------------

def rest(data, player):
    lastRest = Parent.GetUserCooldownDuration(ScriptName, MySet.RestCommand, data.User)
    stmToAdd = int(MySet.RestStm * player.get_max_stm() / 100)
    player.add_stm(stmToAdd)
    hpToAdd = int(MySet.RestHP * player.get_max_hp() / 100)
    player.add_hp(hpToAdd)
    Parent.AddUserCooldown(ScriptName, MySet.RestCommand, data.User, MySet.RestUserCooldown)

def free_rest(player):
    player.add_stm(player.get_max_stm())
    player.add_hp(player.get_max_hp())

#-------------------------------------------------------------
# [Required] Boss database functions
#-------------------------------------------------------------

def get_boss_stats(boss):
    if boss != None:
        boss_stats = "Name: " + boss.Name + " , Level: " + str(boss.Level)
        return boss_stats

def random_boss_by_lvl(c, bossLevel):
    c.execute("SELECT * FROM bosses WHERE level=:level", {'level': bossLevel})
    bossArray = c.fetchone()
    if bossArray != None:
        auxplayer = Player(bossArray[0], bossArray[1], bossArray[2], bossArray[3], bossArray[4], bossArray[5], bossArray[6], bossArray[7], bossArray[8], bossArray[9], bossArray[10])
        return auxplayer
    else:
        return None

#---------------------------------------
# [Optional] Functions for cooldowns
#---------------------------------------
def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

def callBossIsOnCooldown(lvl, data):
    """Return true if command is on cooldown and send cooldown message if enabled"""
    Command = MySet.CallBossCommand
    lvl = int(lvl)
    if (0 < lvl) and (lvl < 6):
        Command += ' [Easy]'
    elif (lvl < 11):
        Command += ' [Medium]'
    elif (lvl < 16):
        Command += ' [Hard]'
    elif (lvl < 21):
        Command += ' [Epic]'
    callBossCooldown = Parent.IsOnCooldown(ScriptName, Command)
    if callBossCooldown is True:
        if MySet.UseBossCooldown:
            bossCDDMinutes = int(Parent.GetCooldownDuration(ScriptName, Command)/60)
            bossCDDSeconds = int(Parent.GetCooldownDuration(ScriptName, Command)%60)
            message = '{' + Command + '}'
            message += MySet.BossOnCooldown.format(bossCDDMinutes, bossCDDSeconds)
            if MySet.Mode == 0:
                Parent.SendStreamWhisper(data.UserName, message)
            elif MySet.Mode == 1:
                Parent.SendDiscordDM(data.UserName, message)
        return True
    return False

def allBossesCooldowns():
    message = '[Easy]: '
    message += callBossCooldown(1)
    message += '[Medium]: '
    message += callBossCooldown(6)
    message += '[Hard]: '
    message += callBossCooldown(11)
    message += '[Epic]: '
    message += callBossCooldown(16)
    return message

def callBossCooldown(lvl):
    """Return true if command is on cooldown and send cooldown message if enabled"""
    Command = MySet.CallBossCommand
    lvl = int(lvl)
    if (0 < lvl) and (lvl < 6):
        Command += ' [Easy]'
    elif (lvl < 11):
        Command += ' [Medium]'
    elif (lvl < 16):
        Command += ' [Hard]'
    elif (lvl < 21):
        Command += ' [Epic]'
    callBossCooldown = Parent.IsOnCooldown(ScriptName, Command)
    if callBossCooldown is True:
        if MySet.UseBossCooldown:
            bossCDDMinutes = int(Parent.GetCooldownDuration(ScriptName, Command)/60)
            bossCDDSeconds = int(Parent.GetCooldownDuration(ScriptName, Command)%60)
            message = str(bossCDDMinutes) + ' minutes, '+ str(bossCDDSeconds) + ' seconds. '
        return message
    return 'ready. '

def dungeonIsOnCooldown(data):
    """Return true if command is on cooldown and send cooldown message if enabled"""
    userCooldown = Parent.IsOnUserCooldown(ScriptName, MySet.DungeonCommand, data.User)
    if userCooldown is True:
        if MySet.UseDungeonCooldown:
            userCDDMinutes = int(Parent.GetUserCooldownDuration(ScriptName, MySet.DungeonCommand, data.User)/60)
            userCDDSeconds = Parent.GetUserCooldownDuration(ScriptName, MySet.DungeonCommand, data.User)%60
            message = MySet.UserDungeonOnCooldown.format(data.UserName, userCDDMinutes, userCDDSeconds)
            if MySet.Mode == 0:
                Parent.SendStreamWhisper(data.UserName, message)
            elif MySet.Mode == 1:
                Parent.SendDiscordDM(data.UserName, message)
        return True
    return False

def restIsOnCooldown(data):
    """Return true if command is on cooldown and send cooldown message if enabled"""
    userCooldown = Parent.IsOnUserCooldown(ScriptName, MySet.RestCommand, data.User)

    if userCooldown is True:
        if MySet.UseRestCooldown:
            userCDDMinutes = int(Parent.GetUserCooldownDuration(ScriptName, MySet.RestCommand, data.User)/60)
            userCDDSeconds = Parent.GetUserCooldownDuration(ScriptName, MySet.RestCommand, data.User)%60
            message = MySet.UserRestOnCooldown.format(data.UserName, userCDDMinutes, userCDDSeconds)
            if MySet.Mode == 0:
                Parent.SendStreamWhisper(data.UserName, message)
            elif MySet.Mode == 1:
                Parent.SendDiscordDM(data.UserName, message)
        return True
    return False

def HasPermission(data):
    """Returns true if user has permission and false if user doesn't"""
    if not Parent.HasPermission(data.User, MySet.Permission, MySet.PermissionResp):
        message = MySet.PermissionResp.format(data.UserName, MySet.Permission, MySet.PermissionResp)
        if MySet.Mode == 0:
            Parent.SendStreamWhisper(data.UserName, message)
        elif MySet.Mode == 1:
            Parent.SendDiscordDM(data.UserName, message)
        return False
    return True

def AddDungeonCooldown(data):
    Parent.AddUserCooldown(ScriptName, MySet.DungeonCommand, data.User, MySet.DungeonCrawlUserCooldown)

def AddRestCooldown(data):
    Parent.AddUserCooldown(ScriptName, MySet.RestCommand, data.User, MySet.RestUserCooldown)

def AddCallBossCooldown(lvl):
    Command = MySet.CallBossCommand
    if (0 < lvl) and (lvl < 6):
        Command += ' [Easy]'
    elif (lvl < 11):
        Command += ' [Medium]'
    elif (lvl < 16):
        Command += ' [Hard]'
    elif (lvl < 21):
        Command += ' [Epic]'
    Parent.AddCooldown(ScriptName, Command, MySet.BossCooldown)